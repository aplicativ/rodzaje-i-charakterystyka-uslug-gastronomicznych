# Deploy na platformę

Dane logowania oraz dokumentację API platformy znajdziesz na Slacku na kanale #contentplus (przypięte wiadomości).

### Kroki do wykonania raz dla projektu (tylko przy pierwszym deploy'u)

1. Stworzenie repozytorium dla komponentu interaktywnego (Menu > Komponenty interaktywne > Dodaj). Nadaj mu nazwę rózniącą się od nazwy repo np. nazwę repozytorium projektu z dopiskiem deploy (tak by uniknąć konfiktu w nazwie repozytoriów).

### By zrobić deploy

1. Pobierz repozytorium (`git clone`) komponentu interaktywnego dla tego projektu (link znajdziesz w Menu > Komponenty interaktywne).
2. Wykonaj komendę `npm run prepareDeploy` w repo projektu.
3. Przekopiuj zawartość powstałego folderu `build` oraz plik `engine.json` do repozytorium komponentu interaktywnego (w repozytorium nie powinno być folderu `build` tylko bezpośrednio jego zawartość).
4. Zrób commit i push.

### By przetestować komponent

1. W materiale edukacyjnym stwórz komponent WOMI. Najpierw wybierz materiał, potem na zakładce Edit przeciągnij komponent WOMI w treść materiału.
2. Stwórz plik `manifest.json` o treści jak poniej. Jako `engine` podaj `code` komponentu interaktywnego, który chcesz uzyć (znajdziesz go w Menu > Komponenty interaktywne).

```json
{
  "engine": "Obszar_I/nazwa_repozytorium_komponentu",
  "dependencies": [],
  "data": {}
}
```

3.  Zrób zip tego pliku.
4.  Przeciągnij zip jako źródło komponentu WOMI, który tworzysz.
5.  Otwórz podgląd materiału edukacyjnego (ikona lupki w menu na górze po prawej).

### Funkcjonalności

Program ćwiczeniowy ma:

1. Zapisanie stanu aplikacji za pomocą przycisku w dolnym menu "Zapisz efekty pracy"
2. Zresetowanie stanu aplikacji za pomocą przycisku "Spróbuj ponownie"
3. Drukowania informacji, pobrania listy kroków za pomocą przycisku "Pobierz listę kroków". Pojawia się w trzech miejscach 1. Menu dolne (ikona strzałki w dół) 2. Popup pozytywny 3. Popup negatywny

Symulator ma:

1. Zapisanie co wykonał uczeń za pomocą przycisku "Pobierz listę kroków". Pojawia się w dwóch miejscach 1. Popup pozytywny 2. Popup negatywny
