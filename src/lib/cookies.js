export const serializeStateList = (state) => {

    let data = {};
    Object.keys(state).map(mainKey => {
        const list = state[mainKey].list;
        data[mainKey] = [];
        list.map(item => {
            data[mainKey].push(item.key);
        })

    })
    return data;
}

export const deserializeStateList = (serializedList, stateList) => {
    let data = {};
    const parsedList = JSON.parse(serializedList);

    const findInState = (itemID) => {
        let item = null;
        Object.keys(stateList).map(key => {
            const list = stateList[key].list;
            list.map(el => {
                if (el.key === itemID) {
                    item = el;
                }
            })

        })
        return item;
    }

    Object.keys(parsedList).map(key => {
        const parsedListArray = parsedList[key];
        data[key] = {};
        data[key]["id"] = key;
        data[key]["list"] = [];

        parsedListArray.map(itemListKey => {

            const item = findInState(itemListKey);

            if (item) {
                data[key]["list"].push(item);
            }
        })
    });
    return data;

}



export const updateCookieState = (key, data) => {
    setCookie(key, JSON.stringify(data));
}

export const isValidState = (appID) => {
    return !(getCookie(appID) === "undefined" || getCookie(appID) == undefined || getCookie(appID) === null || getCookie(appID) === "null")
        ? true
        : false;
}

export const setCookie = (name, val, days, path, domain, secure) => {
    if (navigator.cookieEnabled) {
        const cookieName = encodeURIComponent(name);
        const cookieVal = encodeURIComponent(val);
        let cookieText = cookieName + "=" + cookieVal;

        if (typeof days === "number") {
            const data = new Date();
            data.setTime(data.getTime() + days * 24 * 60 * 60 * 1000);
            cookieText += "; expires=" + data.toGMTString();
        }

        if (path) {
            cookieText += "; path=" + path;
        }
        if (domain) {
            cookieText += "; domain=" + domain;
        }
        if (secure) {
            cookieText += "; secure";
        }

        document.cookie = cookieText;
    }
};

export const getCookie = (name) => {
    if (document.cookie !== "") {
        const cookies = document.cookie.split(/; */);

        for (let i = 0; i < cookies.length; i++) {
            const cookieName = cookies[i].split("=")[0];
            const cookieVal = cookies[i].split("=")[1];
            if (cookieName === decodeURIComponent(name)) {
                return decodeURIComponent(cookieVal);
            }
        }
    }
};
