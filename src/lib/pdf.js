import { jsPDF } from "jspdf";
import html2canvas from "html2canvas";
const printSteps = () => {
  const input = document.getElementById("pdf-file");
  const pdf = new jsPDF("p", "mm", "a4");
  html2canvas(input, {
    onclone: function (clonedDoc) {
      clonedDoc.getElementById("pdf-file").style.display = "block";
    },
  }).then((canvas) => {
    // let imgWidth = 206;
    // let imgHeight = canvas.height * imgWidth / canvas.width;
    // const imgData = canvas.toDataURL('img/png');
    // pdf.addImage(imgData, 'PNG', 2, 1, imgWidth, imgHeight);
    // pdf.save("podsumowanie.pdf");
    var imgData = canvas.toDataURL("image/png");
    /*
            Here are the numbers (paper width and height) that I found to work.
            It still creates a little overlap part between the pages, but good enough for me.
            if you can find an official number from jsPDF, use them.
            */
    var imgWidth = 210;
    var pageHeight = 295;
    var imgHeight = (canvas.height * imgWidth) / canvas.width;
    var heightLeft = imgHeight;
    var doc = new jsPDF("p", "mm");
    var position = 0;
    doc.addImage(imgData, "PNG", 0, position, imgWidth, imgHeight);
    heightLeft -= pageHeight;
    while (heightLeft >= 0) {
      position = heightLeft - imgHeight;
      doc.addPage();
      doc.addImage(imgData, "PNG", 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;
    }
    doc.save("file.pdf");
  });
};
export default printSteps;
