const dataHelper = {
    findItem: (data, key) => data.find(item => item.key == key),
    findNestedItem: (data, key) => {
        let nestedItem = null;
        data.map(section => {
            const { items } = section;
            return items.find(item => item.key == key);
        })
        return nestedItem;
    }
}
export default dataHelper;