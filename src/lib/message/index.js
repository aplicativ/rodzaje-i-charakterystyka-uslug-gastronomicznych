import Swal from 'sweetalert2'
import './message.css'

const Message = {
  success: (title, text) => {
    Swal.fire({
      title,
      text,
      icon: 'success',
      confirmButtonText: 'Zapisz listę kroków',
      showCancelButton: true,
      cancelButtonText: 'Wróć',
      cancelButtonColor: '#357a38',
      backdrop: 'rgba(65,172,38,0.3)',
      showCloseButton: true,
      reverseButtons: true,
    })
  },
  error: (title, text) => {
    Swal.fire({
      title,
      text,
      icon: 'error',
      confirmButtonText: 'Spróbuj jeszcze raz',
      showCloseButton: true,
      reverseButtons: true,
    })
  },
  warning: (title, text) => {
    Swal.fire({
      title,
      text,
      icon: `info`,
      confirmButtonText: `Powrót`,
      showCloseButton: true,
      reverseButtons: true,
    })
  },
}

export default Message
