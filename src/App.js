import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { Button, Dialog } from "@material-ui/core";
import "./index.css";

import CssBaseline from "@material-ui/core/CssBaseline";
import BottomNav from "./components/BottomNavigation";
import DataContext from "./context/DataContext";
import ModalContext from "./context/ModalContext";

import Header from "@components/Header";
import MainNavigation from "@components/MainNavigation";
import Content from "@components/Content";

import Modal from "@components/Modal";
import Alert from "@components/Alert";

import initialState from "@data/initialState";
import { dragAndDropData } from "@data/features";

import useStyles from "./styles";
import { SnackbarProvider } from "notistack";
import { MuiThemeProvider, createTheme } from "@material-ui/core/styles";

import StepsPdf from "@components/StepsPdf";

import DragDropTransfer from "@components/DragDropTransfer";
import { updateCookieState, getCookie, isValidState } from "@lib/cookies";

import printSteps from "@lib/pdf";
import Instruction from "@components/Instruction";
import Info from "@components/Info";

import "./translation";

const theme = createTheme({
  palette: {
    primary: {
      main: "#241B08",
    },
    secondary: {
      main: "#241B08",
    },
  },
  typography: {
    fontFamily: ["OpenSans", "sans-serif"].join(","),
    h2: {
      fontSize: "24px",
      fontWeight: "bold",
      marginBottom: "10px",
      textAlign: "center",
      fontFamily: ["Oswald", "sans-serif"].join(","),
    },
  },
  overrides: {
    MuiTooltip: {
      popper: {
        zIndex: "999999 !important",
      },
    },
  },
});

const modalState = {
  isOpen: false,
  type: null,
  title: null,
  text: null,
};

const alertState = {
  isOpen: false,
  type: null,
  text: null,
};

const App = ({ getImagePath }) => {
  const classes = useStyles();
  const [state, setState] = useState(initialState);
  const [isFullscreen, setIsFullscreen] = useState(false);
  const { level } = state;
  const { [level]: data } = state;
  const [isOpenNavigation, setIsOpenNavigation] = useState(true);
  const [modalParams, setModalParams] = useState(modalState);
  const [alertParams, setAlertParams] = useState(alertState);
  const [columns, setColumns] = useState(dragAndDropData);
  const appID = "rodzajeusluggastronomicznych";
  const [isInstructionOpen, setIsInstructionOpen] = useState(false);
  const [isInfoOpen, setIsInfoOpen] = useState(false);

  const [isPartyEasy, setIsPartyEasy] = useState(false);
  const [isPartyHard, setIsPartyHard] = useState(false);
  const [isDinnerEasy, setIsDinnerEasy] = useState(false);
  const [isDinnerHard, setIsDinnerHard] = useState(false);

  console.log(data)

  useEffect(() => {
    if (isFullscreen) {
      setIsOpenNavigation(true);
    }
  }, [isFullscreen]);

  useEffect(() => {
    if (isValidState(appID)) {
      setState(JSON.parse(getCookie(appID)));
    }
  }, []);

  return (
    <MuiThemeProvider theme={theme}>
      <SnackbarProvider
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
      >
        <ModalContext.Provider
          value={{
            modalParams,
            setModalParams,
            alertParams,
            setAlertParams,
            isInstructionOpen,
            setIsInstructionOpen,
            isInfoOpen,
            setIsInfoOpen,
          }}
        >
          <DataContext.Provider
            value={{
              printSteps,
              state,
              data,
              setState,
              level,
              getImagePath,
              isFullscreen,
              setIsFullscreen,
              columns,
              setColumns,
              appID,
              isPartyEasy,
              setIsPartyEasy,
              isPartyHard,
              setIsPartyHard,
              isDinnerEasy,
              setIsDinnerEasy,
              isDinnerHard,
              setIsDinnerHard,
            }}
          >
            <div
              className={clsx(`${classes.root}`, {
                [classes.fullScreen]: isFullscreen,
              })}
            >
              <CssBaseline />
              <DragDropTransfer>
                <Header
                  isOpenNavigation={isOpenNavigation}
                  setIsOpenNavigation={setIsOpenNavigation}
                />
                <MainNavigation
                  isOpenNavigation={isOpenNavigation}
                  setIsOpenNavigation={setIsOpenNavigation}
                />

                <main
                  className={clsx(classes.content, {
                    [classes.contentShift]: isOpenNavigation,
                  })}
                >
                  <div className={classes.drawerHeader} />
                  <div id="app" className={classes.task}>
                    <Content />
                  </div>

                  <footer
                    className={clsx(classes.footer, {
                      [classes.contentShift]: isOpenNavigation,
                    })}
                  >
                    <BottomNav />
                  </footer>
                </main>
              </DragDropTransfer>
            </div>
            <Modal />
            <Alert />
            <StepsPdf />
            <Instruction />
            <Info />
          </DataContext.Provider>
        </ModalContext.Provider>
      </SnackbarProvider>
    </MuiThemeProvider>
  );
};

export default App;
