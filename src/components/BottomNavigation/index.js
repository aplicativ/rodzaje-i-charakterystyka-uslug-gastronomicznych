import React, { useContext, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import EqualizerIcon from "@material-ui/icons/Equalizer";
import PageviewIcon from "@material-ui/icons/Pageview";
import GetAppIcon from "@material-ui/icons/GetApp";
import SaveIcon from "@material-ui/icons/Save";
import CameraAltIcon from "@material-ui/icons/CameraAlt";
import saveScreen from "@lib/screen";

import initialState from "@data/initialState";
import DataContext from "@context/DataContext";
import SyncIcon from "@mui/icons-material/Sync";
import InfoIcon from "@mui/icons-material/Info";
import { updateCookieState, getCookie } from "@lib/cookies";

import ModalContext from "@context/ModalContext";
import ModalInstruction from "../ModalInstruction";
import SchoolIcon from "@mui/icons-material/School";
import CustomTooltip from "@components/CustomTooltip";
import Tooltip from "@material-ui/core/Tooltip";

const useStyles = makeStyles({
  root: {
    height: "65px",

    "& button": {
      paddingTop: "30px 0",
    },
  },
});

export default function SimpleBottomNavigation() {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const { setState, appID, state, printSteps } = useContext(DataContext);

  const { isInfoOpen, setIsInfoOpen } = useContext(ModalContext);
  const [isInstructionOpen, setIsInstructionOpen] = useState(false);

  const resetState = () => {
    setState({
      level: "easy",

      easy: {
        mainCourse: [],
        soups: [],
        desserts: [],
        coldDrinks: [],
        hotDrinks: [],
        alcoDrinks: [],
        aperitif: [],
        cake: [],
        coldStarters: [],
        hotStarters: [],
        salads: [],
        additives: [],
      },
      average: {
        mainCourse: [],
        soups: [],
        desserts: [],
        coldDrinks: [],
        hotDrinks: [],
        alcoDrinks: [],
        aperitif: [],
        cake: [],
        coldStarters: [],
        hotStarters: [],
        salads: [],
        additives: [],
      },
      hard: {
        mainCourse: [],
        soups: [],
        desserts: [],
        coldDrinks: [],
        hotDrinks: [],
        alcoDrinks: [],
        aperitif: [],
        cake: [],
        coldStarters: [],
        hotStarters: [],
        salads: [],
        additives: [],
      },
      veryHard: {
        mainCourse: [],
        soups: [],
        desserts: [],
        coldDrinks: [],
        hotDrinks: [],
        alcoDrinks: [],
        aperitif: [],
        cake: [],
        coldStarters: [],
        hotStarters: [],
        salads: [],
        additives: [],
      },
    });
    updateCookieState(appID, null);
  };

  return (
    <>
      <ModalInstruction
        isInstructionOpen={isInstructionOpen}
        setIsInstructionOpen={setIsInstructionOpen}
      />

      <BottomNavigation
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
        showLabels
        className={classes.root}
      >
        <Tooltip
          placement="top"
          arrow={true}
          title={
            <span style={{ fontSize: "12px" }}>
              Kliknij, aby zobaczyć instrukcję obsługi programu
            </span>
          }
        >
          <BottomNavigationAction
            label="Instrukcja"
            icon={<InfoIcon />}
            onClick={() => setIsInstructionOpen(true)}
          />
        </Tooltip>
        <Tooltip
          title={
            <span style={{ fontSize: "12px" }}>
              Kliknij, aby zapoznać się z dodatkowymi materiałami oraz
              multimediami
            </span>
          }
          placement="top"
          arrow={true}
        >
          <BottomNavigationAction
            label="Baza wiedzy"
            icon={<SchoolIcon />}
            onClick={() => setIsInfoOpen(true)}
          />
        </Tooltip>
        <Tooltip
          title={
            <span style={{ fontSize: "12px" }}>
              Kliknij, aby pobrać plik PDF z Twoimi wyborami
            </span>
          }
          placement="top"
          arrow={true}
        >
          <BottomNavigationAction
            onClick={() => printSteps()}
            label="Pobierz listę kroków"
            icon={<GetAppIcon />}
          />
        </Tooltip>
        <Tooltip
          title={
            <span style={{ fontSize: "12px" }}>
              Kliknij, aby zapisać postęp pracy. Po zamknięciu karty
              przeglądarki i ponownym jej otwarciu, pozwoli to na powrót do tego
              samego miejsca (powrót do ostatnio dokonanych wyborów)
            </span>
          }
          placement="top"
          arrow={true}
        >
          <BottomNavigationAction
            onClick={() => updateCookieState(appID, state)}
            label="Zapisz efekty pracy"
            icon={<SaveIcon />}
          />
        </Tooltip>

        <Tooltip
          title={
            <span style={{ fontSize: "12px" }}>
              Kliknij, aby zrobić zrzut ekranu"
            </span>
          }
          placement="top"
          arrow={true}
        >
          <BottomNavigationAction
            onClick={() => saveScreen("app")}
            label="Zrzut ekranu"
            icon={<CameraAltIcon />}
          />
        </Tooltip>
        <Tooltip
          title={
            <span style={{ fontSize: "12px" }}>
              Kliknij, aby usunąć wszystkie wybory i ponownie rozwiązać
              ćwiczenie
            </span>
          }
          placement="top"
          arrow={true}
        >
          <BottomNavigationAction
            onClick={() => resetState()}
            label="Spróbuj ponownie"
            icon={<SyncIcon />}
          />
        </Tooltip>
      </BottomNavigation>
    </>
  );
}
