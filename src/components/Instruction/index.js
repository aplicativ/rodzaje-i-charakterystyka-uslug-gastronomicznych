import React, { useContext } from "react";

import { makeStyles } from "@material-ui/core/styles";
import DataContext from "@context/DataContext";

import ModalContext from "@context/ModalContext";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

import AudioPlayer from "./../AudioPlayer";

import urodzEasy from "./../../sounds/impreza_urodzinowa_łatwy.wav";
import urodzHard from "./../../sounds/impreza_urodzinowa_trudny.wav";

import kolacjaEasy from "./../../sounds/uroczysta_kolacja_łatwy.wav";
import kolacjaHard from "./../../sounds/uroczysta_kolacja_trudny.wav";



import urodzEasyEng from "./../../sounds/eng/The_18th_birtday_party_easy.mp3";
import urodzHardEng from "./../../sounds/eng/The_18th_birtday_party_hard.mp3";
import kolacjaEasyEng from "./../../sounds/eng/The_gala_dinner_easy.mp3";
import kolacjaHardEng from "./../../sounds/eng/The_gala_dinner_hard.mp3";

import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: "rgba(65,172,38,0.3)",
  },
  backDrop: {},
  icon: {
    textAlign: "center",
    marginBottom: "10px",
    width: "100%",
  },
  iconImg: {
    margin: "0 auto",
  },
  title: {
    textAlign: "center",
    position: "relative",
    maxWidth: "100%",
    marginBottom: "20px",
    color: "#595959",
    fontSize: "25px",
    fontWeight: "600",
    textAlign: "center",
    textTransform: "none",
    wordWrap: "break-word",
  },
  description: {
    color: "#595959",
    fontWeight: "600",
    textAlign: "center",
    fontSize: "16px",
    marginBottom: "20px",
  },
  buttonsWrap: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    margin: "10px",
  },
}));

const Instruction = () => {
  const { isInstructionOpen, setIsInstructionOpen } = useContext(ModalContext);
  const { level } = useContext(DataContext);

  const classes = useStyles();
  const { t, i18n } = useTranslation("common");
  const handleClose = () => {
    setIsInstructionOpen(false);
  };

  return (
    <>
      <Dialog fullWidth open={isInstructionOpen} onClose={handleClose} style={{ zIndex: "999999" }}>
        {level === "easy" && (
          <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
            <h4 style={{ marginBottom: "20px" }}>{t("Impreza urodzinowa 18-stka")}</h4>
            <div style={{ position: "absolute", top: "15px", right: "15px" }}>
              <IconButton onClick={() => setIsInstructionOpen(false)} aria-label="Example">
                <CloseIcon />
              </IconButton>
            </div>

            <div className={classes.instruction}>
              <ul style={{ marginTop: "20px" }}>
                <p>
                  {t(
                    "Planowana jest impreza z okazji 18 urodzin, w której będzie brać udział 8 osób w wieku 16-18 lat oraz 4 dorosłych. Impreza urodzinowa odbywa się we włoskiej restauracji w sobotę, między godziną 13:00 a 15:00. Jest to przyjęcie bufetowe, które pozwoli gościom na swobodny wybór menu, a także będzie sprzyjało rozmowom, zabawie, czy nawet tańcom."
                  )}
                </p>
                <p>{t("Zamówione urodzinowe menu będzie składało się z następujących elementów:")}</p>
                <li>{t("tzw. bufetu (zimnych i ciepłych przystawek oraz dodatków);")}</li>
                <li>{t("2 dań głównych do wyboru przez gości;")}</li>
                <li>{t("1 deseru;")}</li>
                <li>{t("napojów zimnych;")}</li>
                <li>{t("tortu;")}</li>
                <li>{t("i wina musującego do wzniesienia toastu urodzinowego.")}</li>
              </ul>
              <h3>{t("POZIOM ŁATWY")}</h3>
              <p>{t("Twoim zadaniem jest skomponowanie bufetu, który składać się będzie z:")}</p>
              <ul>
                <li>{t("4 zimnych przystawek,")}</li>
                <li>{t("2 ciepłych przystawek,")}</li>
                <li>{t("1 sałatki,")}</li>
                <li>{t("oraz różnych dodatków.")}</li>
              </ul>
              <p>
                {t(
                  "Pamiętaj, że 18-te urodziny odbywają się we włoskiej restauracji. Wybór przystawek powinien być dopasowany do oferty kuchni włoskiej, ale także do wieku gości i okoliczności imprezy. Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu."
                )}
              </p>
              {i18n.language === "pl" && <AudioPlayer url={urodzEasy} label="Instrukcja obsługi do programu ćwiczeniowego" />}
              {i18n.language === "en" && <AudioPlayer url={urodzEasyEng} label="Instrukcja obsługi do programu ćwiczeniowego" />}
            </div>
          </DialogContent>
        )}
        {level === "average" && (
          <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
            <h4 style={{ marginBottom: "20px" }}>{t("Impreza urodzinowa 18-stka")}</h4>
            <div style={{ position: "absolute", top: "15px", right: "15px" }}>
              <IconButton onClick={() => setIsInstructionOpen(false)} aria-label="Example">
                <CloseIcon />
              </IconButton>
            </div>

            <div className={classes.instruction}>
              <ul style={{ marginTop: "20px" }}>
                <p>
                  {t(
                    "Planowana jest impreza z okazji 18 urodzin, w której będzie brać udział 8 osób w wieku 16-18 lat oraz 4 dorosłych. Impreza urodzinowa odbywa się we włoskiej restauracji w sobotę, między godziną 13:00 a 15:00. Jest to przyjęcie bufetowe, które pozwoli gościom na swobodny wybór menu, a także będzie sprzyjało rozmowom, zabawie, czy nawet tańcom."
                  )}
                </p>
                <p>{t("Zamówione urodzinowe menu będzie składało się z następujących elementów:")}</p>
                <li>{t("tzw. bufetu (zimnych i ciepłych przystawek oraz dodatków);")}</li>
                <li>{t("2 dań głównych do wyboru przez gości;")}</li>
                <li>{t("1 deseru;")}</li>
                <li>{t("napojów zimnych;")}</li>
                <li>{t("tortu;")}</li>
                <li>{t("i wina musującego do wzniesienia toastu urodzinowego.")}</li>
              </ul>
              <h3>{t("POZIOM TRUDNY")}</h3>
              <p>{t("Po wybraniu odpowiednich przystawek i dodatków musisz dobrać:")}</p>
              <ul>
                <li>{t("2 dania główne")}</li>
                <li>{t("1 deser")}</li>
                <li>{t("zimne napoje")}</li>
                <li>{t("oraz 1 tort.")}</li>
              </ul>
              <p>
                {t(
                  "Pamiętaj, że 18-te urodziny odbywają się we włoskiej restauracji. Wybór dań głównych oraz deseru powinien być dopasowany do repertuaru kuchni włoskiej, ale także do wieku gości i okoliczności imprezy. Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu."
                )}
              </p>
              {i18n.language === "pl" && <AudioPlayer url={urodzHard} label="Instrukcja obsługi do programu ćwiczeniowego" />}
              {i18n.language === "en" && <AudioPlayer url={urodzHardEng} label="Instrukcja obsługi do programu ćwiczeniowego" />}
            </div>
          </DialogContent>
        )}
        {level === "hard" && (
          <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
            <h4 style={{ marginBottom: "20px" }}>{t("Uroczysta kolacja")}</h4>
            <div style={{ position: "absolute", top: "15px", right: "15px" }}>
              <IconButton onClick={() => setIsInstructionOpen(false)} aria-label="Example">
                <CloseIcon />
              </IconButton>
            </div>

            <div className={classes.instruction}>
              <ul style={{ marginTop: "20px" }}>
                <p>
                  {t(
                    "Planowana jest uroczysta kolacja, w której będzie brać udział 12 dorosłych osób. Uroczysta kolacja odbywa się w tradycyjnej polskiej restauracji w piątkowy wieczór.Jest to przyjęcie zasiadane z pełną obsługą kelnerską, czyli zimne i ciepłe przystawki, dania główne, desery i napoje są serwowane gościom na stół."
                  )}
                </p>
                <p>{t("Zamówione menu będzie składało się z następujących elementów:")}</p>
                <li>{t("zimnych i ciepłych przystawek, sałatek i dodatków;")}</li>
                <li>{t("1 pierwszego dania;")}</li>
                <li>{t("3 dań głównych do wyboru przez gości;")}</li>
                <li>{t("2 deserów do wyboru przez gości;")}</li>
                <li>{t("napojów zimnych;")}</li>
                <li>{t("napojów gorących;")}</li>
                <li>{t("napojów alkoholowych.")}</li>
              </ul>
              <h3>{t("POZIOM ŁATWY")}</h3>
              <p>
                {t(
                  "Twoim zadaniem jest dobranie aperitifu, który możesz podać gościom oczekującym na przybycie wszystkich zaproszonych gości. Aperitif pełni nie tylko funkcję towarzyską, ale jest również serwowany przed posiłkiem w celu pobudzenia apetytu, gdyż wpływa na produkcję kwasów żołądkowych. Musisz pamiętać, że niektórzy goście mogą wybrać aperitif bezalkoholowy."
                )}
              </p>
              <p>
                {t(
                  "Po wybraniu aperitifu, Twoim zadaniem jest skomponowanie bufetu, który składać się będzie z:"
                )}
              </p>
              <ul>
                <li>{t("5 zimnych przystawek,")}</li>
                <li>{t("5 ciepłych przystawek,")}</li>
                <li>{t("2 sałatek,")}</li>
                <li>{t("oraz różnych dodatków.")}</li>
              </ul>
              <p>
                {t(
                  "Pamiętaj, że to uroczysta kolacja w tradycyjnej polskiej restauracji. Wybór przystawek powinien być dopasowany do oferty kuchni polskiej, ale także do wieku gości i okoliczności imprezy. Przystawki będą podawane na głównym stole, na większych półmiskach. Zwróć uwagę na poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu."
                )}
              </p>
              {i18n.language === "pl" && <AudioPlayer url={kolacjaEasy} label="Instrukcja obsługi do programu ćwiczeniowego" />}
              {i18n.language === "en" && <AudioPlayer url={kolacjaEasyEng} label="Instrukcja obsługi do programu ćwiczeniowego" />}
            </div>
          </DialogContent>
        )}
        {level === "veryHard" && (
          <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
            <h4 style={{ marginBottom: "20px" }}>{t("Uroczysta kolacja")}</h4>
            <div style={{ position: "absolute", top: "15px", right: "15px" }}>
              <IconButton onClick={() => setIsInstructionOpen(false)} aria-label="Example">
                <CloseIcon />
              </IconButton>
            </div>

            <div className={classes.instruction}>
              <ul style={{ marginTop: "20px" }}>
                <p>
                  {t(
                    "Planowana jest uroczysta kolacja, w której będzie brać udział 12 dorosłych osób. Uroczysta kolacja odbywa się w tradycyjnej polskiej restauracji w piątkowy wieczór.Jest to przyjęcie zasiadane z pełną obsługą kelnerską, czyli zimne i ciepłe przystawki, dania główne, desery i napoje są serwowane gościom na stół."
                  )}
                </p>
                <p>{t("Zamówione menu będzie składało się z następujących elementów:")}</p>
                <li>{t("tzw. bufetu (zimnych i ciepłych przystawek);")}</li>
                <li>{t("1 pierwszego dania;")}</li>
                <li>{t("3 dań głównych do wyboru przez gości;")}</li>
                <li>{t("2 deserów do wyboru przez gości;")}</li>
                <li>{t("napojów zimnych;")}</li>
                <li>{t("napojów gorących;")}</li>
                <li>{t("napojów alkoholowych.")}</li>
              </ul>
              <h3>{t("POZIOM TRUDNY")}</h3>
              <p>
                {t(
                  "Po wybraniu aperitifu, odpowiednich przystawek i dodatków musisz dobrać kolejno:"
                )}
              </p>
              <ul>
                <li>{t("1 pierwsze danie,")}</li>
                <li>{t("3 dania główne do wyboru przez gości,")}</li>
                <li>{t("2 desery do wyboru przez gości.")}</li>
              </ul>
              <p>
                {t(
                  "Na koniec będziesz musiał również dokonać selekcji napojów zimnych, gorących oraz alkoholowych. Pamiętaj, że to uroczysta kolacja w tradycyjnej polskiej restauracji. Wybór dań powinien być dopasowany do oferty kuchni polskiej, ale także do wieku gości i okoliczności imprezy. Pierwsze danie podawane będzie w wazie, zaś drugie dania do wyboru wraz z dodatkami będą serwowane na dużych półmiskach. Dzięki temu każdy będzie mógł wybrać coś dla siebie i czuć się swobodnie."
                )}
              </p>
              <p>
                {t("Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu.")}
              </p>
              {i18n.language === "pl" && <AudioPlayer url={kolacjaHard} label="Instrukcja obsługi do programu ćwiczeniowego" />}
              {i18n.language === "en" && <AudioPlayer url={kolacjaHardEng} label="Instrukcja obsługi do programu ćwiczeniowego" />}
            </div>
          </DialogContent>
        )}
      </Dialog>
    </>
  );
};

export default Instruction;
