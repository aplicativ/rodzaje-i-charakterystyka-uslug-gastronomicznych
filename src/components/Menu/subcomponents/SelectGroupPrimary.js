import React, { useContext, useState, useEffect } from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import useStyles from './styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import DataContext from '../../../context/DataContext';
import { updateCookieState, getCookie } from '@lib/cookies';
import { useTranslation } from 'react-i18next';

export default function CheckboxLabels({ title, items, keyData }) {
  const classes = useStyles();
  const { data, setState, state, level, appID } = useContext(DataContext);
  const [isOpen, setIsOpen] = useState(false);
  const { t, i18n } = useTranslation('common');

  items.sort(function (a, b) {
    return a.name.localeCompare(b.name);
  });

  const updateData = (value) => {
    console.log(value);

    let { [keyData]: arrayData } = data;
    if (!arrayData.includes(value)) {
      arrayData.push(value);
    } else {
      arrayData = arrayData.filter((item) => item !== value);
    }

    const newState = {
      ...state,
      [level]: { ...state[level], [keyData]: arrayData },
    };
    setState(newState);
  };

  useEffect(() => {
    data[keyData] ? setIsOpen(true) : setIsOpen(false);
  }, [state]);

  return (
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel2a-content"
        id="panel2a-header"
      >
        <Typography className={classes.heading}>{title}</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.bannedWrap}>
        <FormGroup>
          {items.map((item) => (
            <FormControlLabel
              control={
                <Checkbox
                  checked={data[keyData].includes(item.name) ? true : false}
                  onClick={(e) => updateData(item.name)}
                />
              }
              label={t(item.name)}
              value={item.key}
            />
          ))}
        </FormGroup>
      </AccordionDetails>
    </Accordion>
  );
}
