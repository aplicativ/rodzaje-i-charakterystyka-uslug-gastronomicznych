import React, { useContext } from "react";
import RootRef from "@material-ui/core/RootRef";
// import { DragDropContext, Droppable } from "react-beautiful-dnd";

import { makeStyles } from "@material-ui/core/styles";
import productsData from "@data/products/list";
import {
  mainCourse,
  soups,
  desserts,
  coldDrinks,
  hotDrinks,
  alcoDrinks,
  aperitif,
  cake,
  coldStarters,
  hotStarters,
  salads,
  additives,
} from "@data/features";

import RadioGroupPrimary from "./subcomponents/RadioGroupPrimary";
import SelectGRoupPrimary from "./subcomponents/SelectGroupPrimary";
import DataContext from "@context/DataContext";
import DragDropItem from "@components/DragDropTransfer/subcomponents/item";

import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },

  formControlLabel: {
    "& span": {
      fontSize: "0.8em",
    },
  },
}));

const Menu = () => {
  const { t, i18n } = useTranslation("common");
  const { data, setState, state, level } = useContext(DataContext);
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <>
        <SelectGRoupPrimary
          items={mainCourse}
          keyData="mainCourse"
          title={t("Baza dań głównych")}
          key="mainCourse"
        />
        <SelectGRoupPrimary
          items={soups}
          keyData="soups"
          title={t("Baza dań pierwszych")}
          key="soups"
        />
        <SelectGRoupPrimary
          items={desserts}
          keyData="desserts"
          title={t("Baza deserów")}
          key="desserts"
        />
        <SelectGRoupPrimary
          items={coldDrinks}
          keyData="coldDrinks"
          title={t("Baza zimnych napojów")}
          key="coldDrinks"
        />
        <SelectGRoupPrimary
          items={hotDrinks}
          keyData="hotDrinks"
          title={t("Baza ciepłych napojów")}
          key="hotDrinks"
        />
        <SelectGRoupPrimary
          items={alcoDrinks}
          keyData="alcoDrinks"
          title={t("Baza napojów alkoholowych")}
          key="alcoDrinks"
        />
        <SelectGRoupPrimary
          items={aperitif}
          keyData="aperitif"
          title={t("Baza aperitifów")}
          key="aperitif"
        />
        <SelectGRoupPrimary
          items={cake}
          keyData="cake"
          title={t("Baza tortów")}
          key="cake"
        />
        <SelectGRoupPrimary
          items={coldStarters}
          keyData="coldStarters"
          title={t("Baza zimnych przystawek")}
          key="coldStarters"
        />
        <SelectGRoupPrimary
          items={hotStarters}
          keyData="hotStarters"
          title={t("Baza ciepłych przystawek")}
          key="hotStarters"
        />
        <SelectGRoupPrimary
          items={salads}
          keyData="salads"
          title={t("Baza sałatek")}
          key="salads"
        />
        <SelectGRoupPrimary
          items={additives}
          keyData="additives"
          title={t("Dodatki różne")}
          key="additives"
        />
      </>
    </div>
  );
};

export default Menu;
