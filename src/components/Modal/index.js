import React, { useContext, setState } from "react";
import { Button } from "@material-ui/core";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";

import ModalContext from "@context/ModalContext";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import StepsPdf from "@components/StepsPdf";

import successIcon from "./icons/success.jpg";
import errorIcon from "./icons/error.png";
import warningIcon from "./icons/info.jpg";

import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import DataContext from "../../context/DataContext";

import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: "rgba(65,172,38,0.3)",
  },
  backDrop: {},
  icon: {
    textAlign: "center",
    marginBottom: "10px",
    width: "100%",
  },
  iconImg: {
    margin: "0 auto",
  },
  title: {
    textAlign: "center",
    position: "relative",
    maxWidth: "100%",
    marginBottom: "20px",
    color: "#595959",
    fontSize: "25px",
    fontWeight: "600",
    textAlign: "center",
    textTransform: "none",
    wordWrap: "break-word",
  },
  description: {
    color: "#595959",
    // fontWeight: "600",
    textAlign: "left",
    fontSize: "16px",
    marginBottom: "20px",
  },
  buttonsWrap: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    margin: "10px",
  },
}));

const Modal = () => {
  let { modalParams, setModalParams } = useContext(ModalContext);
  let { printSteps, getImagePath } = useContext(DataContext);
  const { type, title, text, isOpen } = modalParams;
  const { t, i18n } = useTranslation("common");
  
  const classes = useStyles();

  const handleClose = () => {
    setModalParams({
      ...modalParams,
      isOpen: false,
      type: null,
      title: null,
      text: null,
    });
  };

  return (
    <Dialog
      fullWidth
      open={isOpen}
      onClose={handleClose}
      style={{ zIndex: "999999" }}
      BackdropProps={{
        classes: {
          root: type == "success" ? classes.backDropSuccess : classes.backDrop,
        },
      }}
    >
      <div
        onClick={handleClose}
        style={{ position: "absolute", top: "10px", right: "10px" }}
      >
        <IconButton aria-label="Example">
          <CloseIcon />
        </IconButton>
      </div>
      <DialogContent style={{ padding: "25px" }}>
        {type === "success" && (
          <div className={classes.icon}>
            <img className={classes.iconImg} src={successIcon} />
          </div>
        )}
        {type === "error" && (
          <div className={classes.icon}>
            <img className={classes.iconImg} src={errorIcon} />
          </div>
        )}
        {type === "warning" && (
          <div className={classes.icon}>
            <img className={classes.iconImg} src={warningIcon} />
          </div>
        )}

        {title && <div className={classes.title}>{title}</div>}
        {text && <p className={classes.description}>{text}</p>}

        {type === "error" && (
          <ul>
            <li> <a className="link" aria-label="Przejdź do filmu edukacyjnego" target="_blank" href="https://edytor.zpe.gov.pl/a/DyK6OPCB">{t("Przejdź do filmu edukacyjnego")}</a></li>
            <li> <a className="link" aria-label="Przejdź do sekwencji filmowych" target="_blank" href="https://edytor.zpe.gov.pl/x/D17knBWYk?lang=pl&wcag=">{t("Przejdź do sekwencji filmowych")}</a></li>
            <li> <a className="link" aria-label="Przejdź do e-booka" target="_blank" href="https://edytor.zpe.gov.pl/x/D17knBWYk?lang=pl&wcag=">{t("Przejdź do e-booka")}</a></li>
          </ul>
        )}

        <div className={classes.buttonsWrap}>
          {type === "error" && (
            <>
            <Button
              onClick={handleClose}
              variant="contained"
              className={classes.button}
              style={{ backgroundColor: "#d9d9d9", color: "black" }}
            >
              Spróbuj ponownie
            </Button>
               <Button
               onClick={() => printSteps()}
               variant="contained"
               className={classes.button}
               color="primary"
             >
               Pobierz listę kroków
             </Button>
             </>
          )}

          {type === "success" && (
            <>
              <Button
                onClick={handleClose}
                variant="contained"
                className={classes.button}
                style={{ backgroundColor: "#d9d9d9", color: "black" }}
              >
                Powrót
              </Button>
              <Button
                onClick={() => printSteps()}
                variant="contained"
                className={classes.button}
                color="primary"
              >
                Pobierz listę kroków
              </Button>
            </>
          )}

          {type === "warning" && (
            <Button
              onClick={handleClose}
              variant="contained"
              className={classes.button}
              style={{ backgroundColor: "#1979c2", color: "#FFFFFF" }}
            >
              Powrót
            </Button>
          )}
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default Modal;
