import React, { useContext } from "react";
import { Button } from "@material-ui/core";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import DataContex from "../../context/DataContext";
import AudioPlayer from "../AudioPlayer";
import instrukcja from "./../../sounds/instrukcja.mp3";
import instruction from "./../../sounds/eng/instruction.mp3";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import DataContext from "@context/DataContext";

import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: "rgba(65,172,38,0.3)",
  },
  backDrop: {},
  icon: {
    textAlign: "center",
    marginBottom: "10px",
    width: "100%",
  },
  iconImg: {
    margin: "0 auto",
  },
  title: {
    textAlign: "center",
    position: "relative",
    maxWidth: "100%",
    marginBottom: "20px",
    color: "#595959",
    fontSize: "25px",
    fontWeight: "600",
    textAlign: "center",
    textTransform: "none",
    wordWrap: "break-word",
  },
  description: {
    color: "#595959",
    fontWeight: "600",
    textAlign: "center",
    fontSize: "16px",
    marginBottom: "20px",
  },
  buttonsWrap: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    margin: "10px",
  },
  paragraph: {
    lineHeight: 1.5,
    marginBottom: 10,
  },
}));

const Instruction = ({ isInstructionOpen, setIsInstructionOpen }) => {
  const { level } = useContext(DataContex);
  const classes = useStyles();
  const { t, i18n } = useTranslation("common");

  const { isFlavourTable, setIsFlavourTable, setIsKnowledge } =
    useContext(DataContext);

  const handleClose = () => {
    setIsInstructionOpen(false);
  };

  const runTable = () => {
    handleClose();
    setIsFlavourTable(true);
  };
  const runKnowledge = () => {
    handleClose();
    setIsKnowledge(true);
  };

  return (
    <>
      <Dialog
        fullWidth={true}
        maxWidth={"md"}
        open={isInstructionOpen}
        onClose={handleClose}
        style={{ zIndex: "999999" }}
      >
        <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
          <h3 style={{ marginBottom: "20px" }}>{t("instructionTitle")}</h3>
          <div style={{ position: "absolute", top: "15px", right: "15px" }}>
            <IconButton
              onClick={() => setIsInstructionOpen(false)}
              aria-label="Example"
            >
              <CloseIcon />
            </IconButton>
          </div>
          <article className={classes.instruction}>
            <p className={classes.paragraph}>{t("instruction1")}</p>
            <p className={classes.paragraph}>{t("instruction2")}</p>
            <p className={classes.paragraph}>{t("instruction3")}</p>
            <p className={classes.paragraph}>{t("instruction4")}</p>
            <p className={classes.paragraph}>{t("instruction5")}</p>
            <p>{t("instruction6")}</p>
            <p className={classes.paragraph}>
              <div
                dangerouslySetInnerHTML={{
                  __html: t("instructionList"),
                }}
              />
            </p>
            {i18n.language === "pl" && (
              <AudioPlayer
                url={instrukcja}
                label="Instrukcja obsługi do programu ćwiczeniowego"
              />
            )}
            {i18n.language === "en" && (
              <AudioPlayer
                url={instruction}
                label="Instrukcja obsługi do programu ćwiczeniowego"
              />
            )}
          </article>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default Instruction;
