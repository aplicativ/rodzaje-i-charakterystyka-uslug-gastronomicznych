import React, { useContext, useState } from "react";
import "./index.css";

const CustomTooltip = ({ children, title, navbar, submit }) => {
  return (
    <>
      <div
        title={title}
        className={
          navbar ? "navbarToolTip" : submit ? "submitToolTip" : "tooltip"
        }
      >
        {children}
      </div>
    </>
  );
};

export default CustomTooltip;
