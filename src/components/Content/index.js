import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";

import Validation from "@components/Validation";

import DataContext from "@context/DataContext";
import ModalContext from "@context/ModalContext";

import menuPhoto from "../../data/Menu.jpg";
import polMenuPhoto from "../../data/polMenu.jpg";

import menuBcg from "../../data/tlo.jpg";

import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import { menuItems } from "@data/features";
import {
  correctAnswer,
  correctAnswer_easy_coldStarters,
  hotStartersCorrectAnswerEasy,
  saladsCorrectAnswerEasy,
  additivesCorrectAnswerEasy,
  mainCourseCorrectAnswerAverage,
  dessertsCorrectAnswerAverage,
  coldDrinksCorrectAnswerAverage,
  cakeCorrectAnswerAverage,
  aperitifCorrectAnswerHard,
  coldStartersCorrectAnswerHard,
  hotStartersCorrectAnswerHard,
  saladsCorrectAnswerHard,
  additivesCorrectAnswerHard,
  soupsCorrectAnswerVeryHard,
  mainCourseCorrectAnswerVeryHard,
  dessertsCorrectAnswerVeryHard,
  coldDrinksCorrectAnswerVeryHard,
  hotDrinksCorrectAnswerVeryHard,
  alcoDrinksCorrectAnswerVeryHard,
} from "../../data/correctAnswers";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles({
  content: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    padding: "10px",
    margin: "30px auto",
    width: "97%",
    maxWidth: "1100px",
  },
  menuCard: {
    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
    minHeight: "900px",
    width: "600px",
    // backgroundImage: `url(${getImagePath(menuBcg)})`,
    backgroundSize: "1200px",
  },
  contentCard: {
    border: "1px solid grey",
    margin: "40px",
    minHeight: "820px",
    textAlign: "center",
  },
  menuCol: {
    display: "flex",
    flexDirection: "column",
    textAlign: "left",
    margin: "15px 15px",
  },
  menu: {
    display: "grid",
    width: "100%",
    gridTemplateColumns: "1fr 1fr",
  },
});

const Content = () => {
  const classes = useStyles();
  const { data, getImagePath, level, state } = useContext(DataContext);
  const { isInstructionOpen, setIsInstructionOpen, isInfoOpen, setIsInfoOpen } = useContext(ModalContext);
  const { t, i18n } = useTranslation("common");
  const { product } = data;
  const [validOject, setValidObject] = useState({
    easy: false,
    average: false,
    hard: false,
    veryHard: false,
  });

  const renderFeedbackColor = (key) => {
    let color;

    const isValidButtonClicked = data.isValidButtonClicked;

    const newData = { ...data };

    delete newData.isValidButtonClicked;

    if (isValidButtonClicked) {
      color = "red";
    }

    console.log(data)

    const allKeys = Object.keys(newData);
    allKeys.map((keyData) => {
      correctAnswer[level][keyData].forEach((item) => {
        if (isValidButtonClicked) {
          if (item === key) {
            if (correctAnswer[level][keyData].includes(key)) {
              color = "green";
            }
          }
        }
      });
    });

    return color;
  };

  return (
    <>
      <Stack justifyContent="center">
        <Button
          style={{
            width: "97%",
            maxWidth: "700px",
            margin: "40px auto 20px auto",
          }}
          onClick={() => setIsInstructionOpen(true)}
          variant="outlined"
        >
          {t("polecenie", { framework: "React" })}
        </Button>
      </Stack>

      <>
        <div className={classes.content}>
          <div className={classes.menuCard} style={{ backgroundImage: `url(${getImagePath(menuBcg)})` }}>
            <div className={classes.contentCard}>
              <img crossOrigin="true" alt="poster" src={getImagePath(menuPhoto)} style={{ maxWidth: "520px" }} />
              <hr style={{ width: "200px", margin: "revert" }} />
              <div className={classes.menu}>
                {menuItems
                  .filter((item) => state[level][item.keyData].length > 0)
                  .map((item) => (
                    <div className={classes.menuCol}>
                      <h3 style={{ fontSize: "16px" }}>{t(item.name, { framework: "React" })}</h3>

                      <ul>
                        {state[level][item.keyData].map((item) => (
                          <li style={{ color: renderFeedbackColor(item), marginTop: "4px" }}>{t(item)}</li>
                        ))}
                      </ul>
                    </div>
                  ))}
              </div>
            </div>
          </div>
          <div className={classes.content}>
            <Validation />
          </div>
        </div>
      </>
    </>
  );
};

export default Content;
