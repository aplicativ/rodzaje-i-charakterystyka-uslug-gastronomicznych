import React, { useContext} from "react";
import { makeStyles } from "@material-ui/core/styles";
import  DataContext  from "@context/DataContext";
import { typesData } from "@data/features";

import dataHelper from "@lib/data";

const useStyles = makeStyles({
    root: {
    },
});

const Type = () => {
    const classes = useStyles();
    const { data } = useContext(DataContext);
    const type = dataHelper.findItem(typesData, data['type']);
    return (
        <div className={classes.root}>
            {type && (
                <>
                    Typ:
                    {type.name}
                </>
            )}
        </div>
    );
}

export default Type;