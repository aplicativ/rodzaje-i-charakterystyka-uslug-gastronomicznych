import React, { useContext, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

import DataContext from "@context/DataContext";
import productsData from "@data/products/list";
import dataHelper from "@lib/data";

const useStyles = makeStyles({
    image: {
        width: "100%",
        maxWidth: "300px"
    },
});


const Product = () => {
    const classes = useStyles();
    const { data } = useContext(DataContext);
    const product = dataHelper.findItem(productsData, data.product);
    return (
        <div>
            {product && (
                <div>
                    <Typography variant="h2">{product.name}</Typography>
                    <product.image className={classes.image} />
                </div>
            )}
        </div>
    );
}

export default Product;