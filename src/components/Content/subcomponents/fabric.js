import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataContext from "@context/DataContext";
import { fabricsData } from "@data/features";

import dataHelper from "@lib/data";

const useStyles = makeStyles({
    root: {

    },
});


const Fabric = () => {
    const classes = useStyles();
    const { data } = useContext(DataContext);
    const fabric = dataHelper.findNestedItem(fabricsData, data['fabric']);
    return (
        <div className={classes.root}>
            {fabric && (
                <>
                    Tkanina:
                    {fabric.name}
                </>
            )}
        </div>
    );
}

export default Fabric;