import React, { useContext } from "react";

import { makeStyles } from "@material-ui/core/styles";
import DataContext from "@context/DataContext";
import polMenuPhoto from "../../data/polMenu.jpg";

import ModalContext from "@context/ModalContext";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import menuBcg from "../../data/tlo.jpg";
import menuPhoto from "../../data/Menu.jpg";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

import { useTranslation } from "react-i18next";

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: "rgba(65,172,38,0.3)",
  },
  backDrop: {},
  icon: {
    textAlign: "center",
    marginBottom: "10px",
    width: "100%",
  },
  iconImg: {
    margin: "0 auto",
  },
  title: {
    textAlign: "center",
    position: "relative",
    maxWidth: "100%",
    marginBottom: "20px",
    color: "#595959",
    fontSize: "25px",
    fontWeight: "600",
    textAlign: "center",
    textTransform: "none",
    wordWrap: "break-word",
  },
  description: {
    color: "#595959",
    fontWeight: "600",
    textAlign: "center",
    fontSize: "16px",
    marginBottom: "20px",
  },
  buttonsWrap: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    margin: "10px",
  },
  content: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    padding: "10px",
    margin: "30px auto",
    width: "97%",
    maxWidth: "1100px",
  },
  menuCard: {
    boxShadow:
      "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
    height: "800px",
    width: "550px",
    // backgroundImage: `url(${getImagePath(menuBcg)})`,
    backgroundSize: "1200px",
  },
  contentCard: {
    border: "1px solid grey",
    // margin: "40px",
    margin: "15px",
    height: "770px",
    textAlign: "center",
  },
  menuCol: {
    textAlign: "left",
    margin: "0 15px",
    width: "300px",
  },
}));

const Instruction = () => {
  const { isInfoOpen, setIsInfoOpen } = useContext(ModalContext);
  const { level, getImagePath } = useContext(DataContext);
  const { t, i18n } = useTranslation("common");

  const classes = useStyles();

  const handleClose = () => {
    setIsInfoOpen(false);
    // setIsInfoOpen(false);
  };

  return (
    <>
      <Dialog
        fullWidth
        open={isInfoOpen}
        onClose={handleClose}
        style={{ zIndex: "999999" }}
      >



        {level === "easy" && (
          <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
            <div style={{ position: "absolute", top: "10px", right: "15px" }}>
              <IconButton
                onClick={() => setIsInfoOpen(false)}
                aria-label="Example"
              >
                <CloseIcon />
              </IconButton>
            </div>

            <div className={classes.instruction}>
              <ul>
                <li> <a className="link" aria-label="Przejdź do filmu edukacyjnego" target="_blank" href="https://edytor.zpe.gov.pl/a/DyK6OPCB">{t("Przejdź do filmu edukacyjnego")}</a></li>
                <li> <a className="link" aria-label="Przejdź do sekwencji filmowych" target="_blank" href="https://edytor.zpe.gov.pl/x/D17knBWYk?lang=pl&wcag=">{t("Przejdź do sekwencji filmowych")}</a></li>
                <li> <a className="link" aria-label="Przejdź do e-booka" target="_blank" href="https://edytor.zpe.gov.pl/x/D17knBWYk?lang=pl&wcag=">{t("Przejdź do e-booka")}</a></li>
              </ul>
              <p>
                {t("Kuchnia włoska jest bardzo tradycyjna i różnorodna oraz słynie z wykorzystania świeżych składników (mięsa, ryb czy owoców morza), wielu warzyw (np. pomidorów, cebuli czy czosnku), oliwy z oliwek i przypraw (np. oregano, bazylia, pieprz, tymianek, rozmaryn). Tradycyjny posiłek składa się przystawki, pierwszego dania (np. makaronu lub rzadziej zupy), drugiego dania i deseru. 18-te urodziny to doskonała okazja, aby zorganizować je w formie przyjęcia koktajlowego, gdzie wszystkie wybrane dania będą serwowane na stołach bufetowych. Oto przykładowe kompozycje bufetu złożonego z: 4 zimnych przystawek, 2 ciepłych przystawek, 1 sałatki oraz różnych dodatków. Te dania możesz podać gościom, którzy przybędą świętować 18-te urodziny we włoskiej trattorii." )}
              </p>

              <h3 style={{ textAlign: "center" }}>{t("Zestaw nr 1")}</h3>
              <div className={classes.content}>
                <div
                  className={classes.menuCard}
                  style={{ backgroundImage: `url(${getImagePath(menuBcg)})` }}
                >
                  <div className={classes.contentCard}>
                    <img
                      src={getImagePath(menuPhoto)}
                      style={{ maxWidth: "520px" }}
                    />
                    <hr style={{ width: "200px", margin: "revert" }} />
                    <div style={{ display: "flex" }}>
                      <div className={classes.menuCol}>
                        <h3>{t("Zimne przystawki:")}</h3>
                        <ul>
                          <li>{t("Szynka z melonem")}</li>
                          <li>{t("Mozzarella z pomidorami, oliwą i bazylią")}</li>
                          <li>{t("Deska serow włoskich")}</li>
                          <li>{t("Bruschetta")}</li>
                        </ul>
                        <h3>{t("Ciepłe przystawki:")}</h3>
                        <ul>
                          <li>{t("Mule w sosie pomidorowym")}</li>
                          <li>{t("Grillowane warzywa z oliwą")}</li>
                        </ul>
                      </div>
                      <div className={classes.menuCol}>
                        <h3>{t("Sałatki")}</h3>
                        <ul>
                          <li>{t("Sałatka z serem kozim")}</li>
                        </ul>
                        <h3>{t("Dodatki różne")}</h3>
                        <ul>
                          <li>{t("Pieczywo ciemne")}</li>
                          <li>{t("Grissini")}</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <h3 style={{ textAlign: "center" }}>{t("Zestaw nr 2")}</h3>
            <div className={classes.content}>
              <div
                className={classes.menuCard}
                style={{ backgroundImage: `url(${getImagePath(menuBcg)})` }}
              >
                <div className={classes.contentCard}>
                  <img
                    src={getImagePath(menuPhoto)}
                    style={{ maxWidth: "520px" }}
                  />
                  <hr style={{ width: "200px", margin: "revert" }} />
                  <div style={{ display: "flex" }}>
                    <div className={classes.menuCol}>
                      <h3>{t("Zimne przystawki:")}</h3>
                      <ul>
                        <li>{t("Deska wędlin włoskich")}</li>
                        <li>{t("Mozzarella z pomidorami, oliwą i bazylią")}</li>
                        <li>{t("Zielone oliwki")}</li>
                        <li>{t("Bruschetta")}</li>
                      </ul>
                      <h3>{t("Ciepłe przystawki:")}</h3>
                      <ul>
                        <li>{t("Calzone")}</li>
                        <li>{t("Sycylijska caponata")}</li>
                      </ul>
                    </div>
                    <div className={classes.menuCol}>
                      <h3>{t("Sałatki")}</h3>
                      <ul>
                        <li>{t("Sałatka z grillowanym łososiem")}</li>
                      </ul>
                      <h3>{t("Dodatki różne")}</h3>
                      <ul>
                        <li>{t("Grissini")}</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <h4>
                {t("Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie, zarówno tacy, którzy przepadają za mięsem oraz tacy, którzy preferują dietę wegetariańską. Warto zwrócić uwagę, aby w zestawie pojawiły się produkty pełnoziarniste, warzywa i ryby.")}
              </h4>
            </div>
          </DialogContent>
        )}{" "}
        {level === "average" && (
          <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
            <div className={classes.instruction}>
              <p>
                {t("Dla kuchni włoskiej bardzo charakterystyczne są potrawy mączne (np. makarony, ravioli czy tortellini), które w towarzystwie sosów stają się dość kaloryczne. Dlatego może warto sięgnąć np. po makaron pełnoziarnisty lub wybrać ryby i warzywa.")}
              </p>
              <p>
                {t("18-te urodziny to doskonała okazja, aby zorganizować je w formie przyjęcia koktajlowego, gdzie wszystkie wybrane dania będą serwowane na stołach bufetowych. Oto przykładowe kompozycje bufetu złożonego z: 2 dań głównych, deseru, zimnych napojów oraz tortu urodzinowego. Te dania możesz podać gościom, którzy przybędą świętować 18-te urodziny we włoskiej trattorii.")}
              </p>
              <h3 style={{ textAlign: "center" }}>{t("Zestaw nr 1")}</h3>
              <div className={classes.content}>
                <div
                  className={classes.menuCard}
                  style={{ backgroundImage: `url(${getImagePath(menuBcg)})` }}
                >
                  <div className={classes.contentCard}>
                    <img
                      src={getImagePath(menuPhoto)}
                      style={{ maxWidth: "520px" }}
                    />
                    <hr style={{ width: "200px", margin: "revert" }} />
                    <div style={{ display: "flex" }}>
                      <div className={classes.menuCol}>
                        <h3>{t("Dania główne:")}</h3>
                        <ul>
                          <li>{t("Penne z kurczakiem i szpinakiem")}</li>
                          <li>{t("Pizza Margherita")}</li>
                        </ul>
                        <h3>{t("Desery")}</h3>
                        <ul>
                          <li>{t("Lody cytrynowe")}</li>
                        </ul>
                      </div>
                      <div className={classes.menuCol}>
                        <h3>{t("Zimne napoje:")}</h3>
                        <ul>
                          <li>{t("Woda gazowana")}</li>
                          <li>{t("Woda niegazowana")}</li>
                          <li>{t("Sok pomarańczowy")}</li>
                        </ul>
                        <h3>{t("Torty")}</h3>
                        <ul>
                          <li>{t("Tort tiramisu")}</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <h3 style={{ textAlign: "center" }}>{t("Zestaw nr 2")}</h3>
            <div className={classes.content}>
              <div
                className={classes.menuCard}
                style={{ backgroundImage: `url(${getImagePath(menuBcg)})` }}
              >
                <div className={classes.contentCard}>
                  <img
                    src={getImagePath(menuPhoto)}
                    style={{ maxWidth: "520px" }}
                  />
                  <hr style={{ width: "200px", margin: "revert" }} />
                  <div style={{ display: "flex" }}>
                    <div className={classes.menuCol}>
                      <h3>{t("Dania główne:")}</h3>
                      <ul>
                        <li>{t("Ravioli")}</li>
                        <li>{t("Polędwiczki wieprzowe z ziemniakami")}</li>
                      </ul>
                      <h3>{t("Desery")}</h3>
                      <ul>
                        <li>{t("Panna cotta")}</li>
                      </ul>
                    </div>
                    <div className={classes.menuCol}>
                      <h3>{t("Zimne napoje:")}</h3>
                      <ul>
                        <li>{t("Woda gazowana")}</li>
                        <li>{t("Woda niegazowana")}</li>
                        <li>{t("Sok pomarańczowy")}</li>
                      </ul>
                      <h3>{t("Torty")}</h3>
                      <ul>
                        <li>{t("Tort cytrynowy")}</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <h4>
              {t("Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie. Miłośnicy mięsa oraz wegetarianie powinni być zadowoleni. Warto zwrócić uwagę, aby nie pojawiały się kolorowe napoje gazowane, można też zastanowić się nad rezygnacją z deseru, skoro na koniec podany zostanie tort urodzinowy. Postarajmy się skomponować różnorodne menu.")}
              </h4>
            </div>
          </DialogContent>
        )}
        {level === "hard" && (
          <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
            <div className={classes.instruction}>
              <p>
                {t("Tradycyjna kuchnia polska oferuje duży repertuar różnie przygotowanych przystawek zimnych i ciepłych (np. śledzie, tatar, jajka faszerowane) oraz rozmaitych dodatków (np. ogórki kiszone, dynia czy grzybki marynowane).")}
              </p>
              <p>
                {t("Uroczysta kolacja to doskonała okazja, aby przyjęcie miało charakter zasiadany. Warto zatem pomyśleć o odpowiednio dobranej zastawie i bieliźnie stołowej, a także eleganckiej dekoracji stołów. Goście obsługiwani będą przez kelnerów." )}
              </p>
              <p>
                {t( "Oto przykładowa kompozycja bufetu złożonego z: 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatek oraz różnych dodatków. Te dania możesz podać gościom, którzy przybędą na uroczystą kolację do tradycyjnej polskiej restauracji.")}
              </p>
              <h3 style={{ textAlign: "center" }}>{t("Bufet nr 1")}</h3>
              <div className={classes.content}>
                <div
                  className={classes.menuCard}
                  style={{ backgroundImage: `url(${getImagePath(menuBcg)})` }}
                >
                  <div className={classes.contentCard}>
                    <img
                      src={getImagePath(polMenuPhoto)}
                      style={{ maxWidth: "520px" }}
                    />
                    <hr style={{ width: "200px", margin: "revert" }} />
                    <div style={{ display: "flex" }}>
                      <div className={classes.menuCol}>
                        <h3>{t("Przystawki zimne:")}</h3>
                        <ul>
                          <li>{t("Deska wędlin")}</li>
                          <li>{t("Deska serów polskich")}</li>
                          <li>{t("Śledzie w śmietanie")}</li>
                          <li>{t("Tatar wołowy")}</li>
                          <li>
                            {t("Placki warzywne z cukinią i wędzonym pstrągiem")}
                          </li>
                        </ul>
                        <h3>{t("Przystawki ciepłe:")}</h3>
                        <ul>
                          <li>{t("Grillowane warzywa z oliwą")}</li>
                          <li>{t("Krokiety z mięsem")}</li>
                          <li>{t("Pierogi z grzybami")}</li>
                          <li>{t("Śliwki zawijane w boczku")}</li>
                          <li>{t("Jajka faszerowane")}</li>
                        </ul>
                      </div>
                      <div className={classes.menuCol}>
                        <h3>{t("Sałatki")}</h3>
                        <ul>
                          <li>{t("Sałatka jarzynowa")}</li>
                          <li>{t("Sałatka z pieczonego buraka z serem kozim")}</li>
                        </ul>
                        <h3>{t("Dodatki różne")}</h3>
                        <ul>
                          <li>{t("Pieczywo ciemne")}</li>
                          <li>{t("Grzybki marynowane")}</li>
                          <li>{t("Dynia w zalewie")}</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.content}>
              <h4>
             {t("Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie, zarówno tacy, którzy przepadają za mięsem oraz tacy, którzy preferują dietę wegetariańską. Warto zwrócić uwagę, aby w zestawie pojawiły się produkty pełnoziarniste, warzywa i ryby.")}
              </h4>
            </div>
          </DialogContent>
        )}
        {level === "veryHard" && (
          <DialogContent style={{ padding: "25px 25px 25px 25px" }}>
            <div className={classes.instruction}>
              <p>
               {t("W tradycyjnej kuchni polskiej dominują potraw z mięsa i różnych warzyw (np. kapusty, ziemniaków), a także ciekawe desery (np. szarlotka, sernik czy pączki). Jej cechą charakterystyczną jest także duża oferta zup.")}
              </p>
              <p>
                {t("Uroczysta kolacja to doskonała okazja, aby przyjęcie miało charakter zasiadany. Warto zatem pomyśleć o odpowiednio dobranej zastawie i bieliźnie stołowej, a także eleganckiej dekoracji stołów. Goście obsługiwani będą przez kelnerów.")}
              </p>
              <p>
                {t("Oto przykładowa kompozycja menu złożonego z: 1 pierwszego dania, 3 dań głównych do wyboru; 2 deserów do wyboru oraz napojów zimnych, gorących oraz alkoholowych. Te dania możesz podać gościom, którzy przybędą na uroczystą kolację do tradycyjnej polskiej restauracji.")}
              </p>
              <h3 style={{ textAlign: "center" }}>{t("Zestaw nr 1")}</h3>
              <div className={classes.content}>
                <div
                  className={classes.menuCard}
                  style={{ backgroundImage: `url(${getImagePath(menuBcg)})` }}
                >
                  <div className={classes.contentCard}>
                    <img
                      src={getImagePath(polMenuPhoto)}
                      style={{ maxWidth: "520px" }}
                    />
                    <hr style={{ width: "200px", margin: "revert" }} />
                    <div style={{ display: "flex" }}>
                      <div className={classes.menuCol}>
                        <h3>{t("Dania pierwsze:")}</h3>
                        <ul>
                          <li>
                            {t("Barszcz czerwony z uszkami z nadzieniem z borowików")}
                          </li>
                        </ul>
                        <h3>{t("Dania główne:")}</h3>
                        <ul>
                          <li>{t("Kaczka z jabłkami i sosem żurawinowym")}</li>
                          <li>{t("Gulasz z dzika z plasterkami marchewki")}</li>
                          <li>{t("Placek ziemniaczany z gulaszem z warzyw")}</li>
                        </ul>
                      </div>
                      <div className={classes.menuCol}>
                        <h3>{t("Desery")}</h3>
                        <ul>
                          <li>{t("Szarlotka z lodami")}</li>
                        </ul>
                        <h3>{t("Zimne napoje:")}</h3>
                        <ul>
                          <li>{t("Woda gazowana")}</li>
                          <li>{t("Woda niegazowana")}</li>
                          <li>{t("Sok pomarańczowy")}</li>
                        </ul>
                        <h3>{t("Ciepłe napoje:")}</h3>
                        <ul>
                          <li>{t("Espresso")}</li>
                          <li>{t("Herbata czarna")}</li>
                        </ul>
                        <h3>{t("Napoje alkoholowe:")}</h3>
                        <ul>
                          <li>{t("Wino czerwone półwytrawne")}</li>
                          <li>{t("Wódka")}</li>
                          <li>{t("Piwo")}</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className={classes.content}>
              <h4>
                {t("Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie. Miłośnicy mięsa oraz wegetarianie powinni być zadowoleni. Warto zwrócić uwagę, aby nie pojawiały się kolorowe napoje gazowane. Postarajmy się skomponować różnorodne menu.")}
              </h4>
            </div>
          </DialogContent>
        )}
      </Dialog>
    </>
  );
};

export default Instruction;
