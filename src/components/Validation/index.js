import React, { useContext, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DataContex from '../../context/DataContext';
import { Button } from '@material-ui/core';
import ModalCalculator from './../ModalCalculator';
import dataHelper from '@lib/data';
import CustomTooltip from '@components/CustomTooltip';
import productsData from '@data/products/list';
import ModalContext from '../../context/ModalContext';
import Tooltip from '@material-ui/core/Tooltip';

import { useTranslation } from 'react-i18next';

//informacja zwrotna pozytywna - uzupełniamy tekstem od redaktora
let successInfo =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus dignissim nisl consectetur enim semper hendrerit sed feugiat nisl. Sed id diam a velit congue feugiat. Suspendisse ac nulla euismod, volutpat lectus ut, congue erat.';
//informacja zwrotna negatywna - uzupełniamy tekstem od redaktora
let errorInfo =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus dignissim nisl consectetur enim semper hendrerit sed feugiat nisl. Sed id diam a velit congue feugiat. Suspendisse ac nulla euismod, volutpat lectus ut, congue erat.';

const useStyles = makeStyles((theme) => ({
  wrap: {
    marginTop: '10px',
  },
}));

const Validation = () => {
  const classes = useStyles();
  const { t, i18n } = useTranslation('common');

  const {
    data,
    level,
    setIsPartyEasy,
    setIsPartyHard,
    setIsDinnerEasy,
    setIsDinnerHard,
    state,
    setState,
  } = useContext(DataContex);
  const { product, type, fabric } = data;

  const [isOpen, setIsOpen] = useState(false);

  let { setModalParams } = useContext(ModalContext);
  const initModal = ({ type, title, text }) => {
    setModalParams({ type, title, text, isOpen: true });
  };

  const coldStartersNames = state.easy.coldStarters;
  const hotStartersNames = state.easy.hotStarters;
  const dessertsNames = state.average.desserts;
  const desserts2Names = state.veryHard.desserts;
  const hotStarters2Names = state.hard.hotStarters;
  const hotDrinksNames = state.veryHard.hotDrinks;
  const saladsNames = state.easy.salads;
  const salads2Names = state.hard.salads;
  const alcoDrinksNames = state.veryHard.alcoDrinks;
  const coldDrinksNames = state.average.coldDrinks;
  const coldDrinks2Names = state.veryHard.coldDrinks;
  const coldStarters2Names = state.hard.coldStarters;
  const mainCourseNames = state.average.mainCourse;
  const mainCourse2Names = state.veryHard.mainCourse;
  const additivesNames = state.easy.additives;
  const additives2Names = state.hard.additives;
  const soupsNames = state.veryHard.soups;
  const aperitifNames = state.hard.aperitif;
  const cakeNames = state.average.cake;

  const productInfo = dataHelper.findItem(productsData, data.product);
  const warningInfo = 'Nie wybrałeś wszystkich elementów';

  const isValid = () => {
    let isValid = false;
    if (level === 'easy') {
      if (
        coldStartersNames.length === 0 &&
        hotStartersNames.length === 0 &&
        saladsNames.length === 0 &&
        additivesNames.length === 0
      ) {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          // title: warningInfo,
          text: t(
            'Niestety, nie wszystko poszło dobrze. Bufet na imprezę urodzinową we włoskiej trattoria jest niekompletny. Zwróć uwagę czy na pewno wybrałeś wszystkie przystawki zimne, ciepłe, sałatki i dodatki, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy.'
          ),
        });
      } else {
        if (
          data.soups.length > 0 ||
          data.mainCourse.length > 0 ||
          data.desserts.length > 0 ||
          data.coldDrinks.length > 0 ||
          data.hotDrinks.length > 0 ||
          data.aperitif.length > 0 ||
          data.alcoDrinks.length > 0 ||
          data.cake.length > 0 ||
          // !data.coldStarters.includes("Szynka z melonem") ||
          // !data.coldStarters.includes("Deska wędlin") ||
          // !data.coldStarters.includes("Deska serów włoskich") ||
          // !data.coldStarters.includes("Czarne oliwki") ||
          // !data.coldStarters.includes("Zielone oliwki") ||
          // !data.coldStarters.includes("Bruschetta") ||
          // !data.coldStarters.includes("Mozzarella z oliwą i bazylią") ||
          // !data.coldStarters.includes("Carpaccio") ||
          data.coldStarters.length !== 4 ||
          data.coldStarters.includes('Dolmadakia - greckie gołąbki') ||
          data.coldStarters.includes('Śledzie w śmietanie') ||
          data.coldStarters.includes('Śledzie w sosie curry') ||
          data.coldStarters.includes('Wędzona makrela') ||
          data.coldStarters.includes('Ryba po grecku') ||
          data.coldStarters.includes('Karp w galarecie') ||
          data.coldStarters.includes('Indyk w galarecie') ||
          data.coldStarters.includes('Deska serów polskich') ||
          data.coldStarters.includes('Deska wędlin') ||
          data.coldStarters.includes(
            'Placki warzywne z cukinią i wędzonym pstrągiem'
          ) ||
          data.coldStarters.includes('Ryba po grecku') ||
          data.coldStarters.includes('Tatar wołowy') ||
          // !data.hotStarters.includes("Mule w sosie pomidorowym") ||
          // !data.hotStarters.includes("Kalmary smażone") ||
          // !data.hotStarters.includes("Calzone") ||
          // !data.hotStarters.includes("Sycylijska caponata") ||
          // !data.hotStarters.includes("Grillowane warzywa z oliwą") ||
          data.hotStarters.length !== 2 ||
          data.hotStarters.includes('Placek chaczapuri') ||
          data.hotStarters.includes('Sajgonki') ||
          data.hotStarters.includes('Pierożki wonton') ||
          data.hotStarters.includes('Bigos') ||
          data.hotStarters.includes('Placki ziemniaczane') ||
          data.hotStarters.includes('Quiche lorraine') ||
          data.hotStarters.includes('Tortilla z mięsem mielonym') ||
          data.hotStarters.includes('Krokiety z pieczarkami') ||
          data.hotStarters.includes('Krokiety z mięsem') ||
          data.hotStarters.includes('Jajka faszerowane') ||
          data.hotStarters.includes('Pierogi z grzybami') ||
          data.hotStarters.includes('Śliwki zawijane w boczku') ||
          // !data.salads.includes("Sałatka z grillowanym łososiem") ||
          // !data.salads.includes("Sałatka z serem kozim") ||
          // !data.salads.includes("Sałatka caprese") ||
          data.salads.length !== 1 ||
          data.salads.includes('Sałatka z oscypkiem') ||
          data.salads.includes('Sałatka jarzynowa') ||
          data.salads.includes('Sałatka z kaszą gryczaną i warzywami') ||
          data.salads.includes('Gyros') ||
          data.salads.includes('Sałatka orientalna z krewetkami') ||
          data.salads.includes('Sałatka z kaszą gryczaną i warzywami') ||
          data.salads.includes('Sałatka z pieczonego buraka z serem kozim') ||
          // !data.additives.includes("Pieczywo jasne") ||
          // !data.additives.includes("Pieczywo ciemne") ||
          // !data.additives.includes("Grissini") ||
          data.additives.length < 1 ||
          data.additives.includes('Ryż') ||
          data.additives.includes('Chipsy') ||
          data.additives.includes('Dynia w zalewie') ||
          data.additives.includes('Frytki') ||
          data.additives.includes('Grzybki marynowane') ||
          data.additives.includes('Makaron smażony') ||
          data.additives.includes('Ogórki kiszone') ||
          data.additives.includes('Ziemniaki gotowane') ||
          (data.additives.includes('Kluski śląskie') &&
            (!data.coldStarters.includes('Szynka z melonem') ||
              !data.coldStarters.includes('Deska wędlin włoskich') ||
              !data.coldStarters.includes('Deska serów włoskich') ||
              !data.coldStarters.includes('Czarne oliwki') ||
              !data.coldStarters.includes('Zielone oliwki') ||
              !data.coldStarters.includes('Bruschetta') ||
              !data.coldStarters.includes(
                'Mozzarella z pomidorami, oliwą i bazylią'
              ) ||
              !data.coldStarters.includes('Carpaccio')) &&
            (!data.hotStarters.includes('Mule w sosie pomidorowym') ||
              !data.hotStarters.includes('Kalmary smażone') ||
              !data.hotStarters.includes('Calzone') ||
              !data.hotStarters.includes('Sycylijska caponata') ||
              !data.hotStarters.includes('Grillowane warzywa z oliwą')) &&
            (!data.salads.includes('Sałatka z grillowanym łososiem') ||
              !data.salads.includes('Sałatka z serem kozim') ||
              !data.salads.includes('Sałatka caprese')) &&
            (!data.additives.includes('Pieczywo jasne') ||
              !data.additives.includes('Pieczywo ciemne') ||
              !data.additives.includes('Grissini')))
        ) {
          setIsPartyEasy(false);
          initModal({
            type: 'error',
            title: 'Niestety...',
            text: t('error1'),
          });
        } else {
          setIsPartyEasy(true);
          initModal({
            type: 'success',
            title: 'Gratulacje!',
            text: t(
              'Ten poziom został wykonany poprawnie. Doskonale poradziłeś sobie z wyborem 4 zimnych i 2 ciepłych przystawek, 1 sałatki oraz dodatków części menu na imprezę urodzinową dla 8 osób w wieku 16-18 lat oraz 4 osób dorosłych, która odbywa się we włoskiej trattorii. Świetnie!'
            ),
          });
        }
      }
    } else if (level === 'average') {
      if (
        mainCourseNames.length === 0 &&
        dessertsNames.length === 0 &&
        coldDrinksNames.length === 0 &&
        cakeNames.length === 0
      ) {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          text: t(
            'Niestety, nie wszystko poszło dobrze. Główna część menu (dania główne, desery, napoje i  tort) na imprezę urodzinową we włoskiej trattoria jest niekompletna. Zastanów się czy na pewno wybrałeś wszystkie elementy menu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy.'
          ),
        });
      } else {
        if (
          data.soups.length > 0 ||
          data.coldStarters.length > 0 ||
          data.hotStarters.length > 0 ||
          data.salads.length > 0 ||
          data.additives.length > 0 ||
          data.alcoDrinks.length > 0 ||
          data.hotDrinks.length > 0 ||
          data.aperitif.length > 0 ||
          // !data.mainCourse.includes("Gnocchi z sosem serowym") ||
          // !data.mainCourse.includes("Ravioli z borowikami") ||
          // !data.mainCourse.includes("Tagiatelle alla carbonara") ||
          // !data.mainCourse.includes("Penne z kurczakiem i szpinakiem") ||
          // !data.mainCourse.includes("Pizza Margherita") ||
          // !data.mainCourse.includes("Polędwiczki wieprzowe z ziemniakami") ||
          data.mainCourse.length !== 2 ||
          data.mainCourse.includes('Barszcz') ||
          data.mainCourse.includes('Pierogi z jagodami') ||
          data.mainCourse.includes('Chiński smażony ryż z warzywami') ||
          data.mainCourse.includes('Mandaryńskie placki') ||
          data.mainCourse.includes('Tofu z warzywami z woka') ||
          data.mainCourse.includes('Musaka') ||
          data.mainCourse.includes('Gyros z jagnięciny') ||
          data.mainCourse.includes('Chłodnik litewski') ||
          data.mainCourse.includes('Wołowina po burgundzku') ||
          data.mainCourse.includes('Gulasz z dzika z plasterkami marchewki') ||
          data.mainCourse.includes('Kaczka z jabłkami i sosem żurawinowym') ||
          data.mainCourse.includes('Pielmieni ze szpinakiem') ||
          data.mainCourse.includes('Placek ziemniaczany z gulaszem z warzyw') ||
          // !data.desserts.includes("Tiramisu") ||
          // !data.desserts.includes("Panna cotta") ||
          // !data.desserts.includes("Lody cytrynowe") ||
          data.desserts.length !== 1 ||
          data.desserts.includes('Strudel jabłkowy') ||
          data.desserts.includes('Zupa owocowa') ||
          data.desserts.includes('Kutia') ||
          data.desserts.includes('Szarlotka z lodami') ||
          data.desserts.includes('Racuchy') ||
          data.desserts.includes('Smażone banany w cieście') ||
          data.desserts.includes('Krem bawarski') ||
          data.desserts.includes('Eklerka z czekoladą') ||
          data.desserts.includes('Creme brulèe') ||
          // !data.coldDrinks.includes("Woda gazowana") ||
          // !data.coldDrinks.includes("Woda niegazowana") ||
          // !data.coldDrinks.includes("Sok pomarańczowy") ||
          data.coldDrinks.length < 1 ||
          data.coldDrinks.includes('Napój energetyczny') ||
          data.coldDrinks.includes('Napój kolorowy gazowany') ||
          data.coldDrinks.includes('Herbata') ||
          // !data.cake.includes("Tort tiramisu") ||
          // !data.cake.includes("Tort migdałowy") ||
          // !data.cake.includes("Tort cytrynowy") ||
          data.cake.length !== 1 ||
          data.cake.includes('Tort makowy') ||
          data.cake.includes('Tort piernikowy') ||
          (data.cake.includes('Tort a la Sacher') &&
            (!data.mainCourse.includes('Gnocchi z sosem serowym') ||
              !data.mainCourse.includes('Ravioli z borowikami') ||
              !data.mainCourse.includes('Tagiatelle alla carbonara') ||
              !data.mainCourse.includes('Penne z kurczakiem i szpinakiem') ||
              !data.mainCourse.includes('Pizza Margherita') ||
              !data.mainCourse.includes(
                'Polędwiczki wieprzowe z ziemniakami'
              )) &&
            (!data.desserts.includes('Tiramisu') ||
              !data.desserts.includes('Panna cotta') ||
              !data.desserts.includes('Lody cytrynowe')) &&
            (!data.coldDrinks.includes('Woda gazowana') ||
              !data.coldDrinks.includes('Woda niegazowana') ||
              !data.coldDrinks.includes('Sok pomarańczowy')) &&
            (!data.cake.includes('Tort tiramisu') ||
              !data.cake.includes('Tort migdałowy') ||
              !data.cake.includes('Tort cytrynowy')))
        ) {
          setIsPartyHard(false);

          initModal({
            type: 'error',
            title: 'Niestety...',
            text: t('error2'),
          });
        } else {
          setIsPartyHard(true);

          initModal({
            type: 'success',
            title: 'Gratulacje!',
            text: t(
              'Ćwiczenie zostało rozwiązane poprawnie. Poszczególne części menu imprezy urodzinowej dla 8 osób w wieku 16-18 lat oraz 4 osób dorosłych, która odbywa się we włoskiej trattorii, zostały prawidłowo skomponowane. Świetnie!'
            ),
          });
        }
      }
    } else if (level === 'hard') {
      if (
        aperitifNames.length === 0 &&
        coldStarters2Names.length === 0 &&
        hotStarters2Names.length === 0 &&
        salads2Names.length === 0 &&
        additives2Names.length === 0
      ) {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          text: t(
            'Niestety, nie wszystko poszło dobrze. Aperitif oraz bufet na uroczystą kolację w tradycyjnej polskiej restauracji są niekompletne. Zastanów się czy na pewno wybrałeś napoje na aperitif oraz wszystkie elementy bufetu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy.'
          ),
        });
      } else {
        if (
          data.mainCourse.length > 0 ||
          data.soups.length > 0 ||
          data.desserts.length > 0 ||
          data.coldDrinks.length > 0 ||
          data.hotDrinks.length > 0 ||
          data.alcoDrinks.length > 0 ||
          data.cake.length > 0 ||
          data.aperitif.length < 1 ||
          data.aperitif.includes('Limoncello') ||
          data.aperitif.includes('Ziołowe digestivo') ||
          data.aperitif.includes('Wino wytrawne') ||
          data.aperitif.includes('Krem czekoladowy') ||
          data.aperitif.includes('Ajerkoniak') ||
          data.coldStarters.length !== 5 ||
          data.coldStarters.includes('Czarne oliwki') ||
          data.coldStarters.includes('Zielone oliwki') ||
          data.coldStarters.includes('Bruschetta') ||
          data.coldStarters.includes('Carpaccio') ||
          data.coldStarters.includes('Dolmadakia - greckie gołąbki') ||
          data.coldStarters.includes('Ryba po grecku') ||
          data.coldStarters.includes('Szynka z melonem') ||
          data.coldStarters.includes('Karp w galarecie') ||
          data.coldStarters.includes(
            'Mozzarella z pomidorami, oliwą i bazylią'
          ) ||
          data.coldStarters.includes('Śledzie w sosie curry') ||
          data.coldStarters.includes('Wędzona makrela') ||
          data.coldStarters.includes('Deska serów włoskich') ||
          data.coldStarters.includes('Deska wędlin włoskich') ||
          // !data.hotStarters2.includes("Grillowane warzywa z oliwą") ||
          // !data.hotStarters2.includes("Krokiety z mięsem") ||
          // !data.hotStarters2.includes("Pierogi z grzybami") ||
          // !data.hotStarters2.includes("Śliwki zawijane w boczku") ||
          // !data.hotStarters2.includes("Jajka faszerowane") ||
          data.hotStarters.length !== 5 ||
          data.hotStarters.includes('Placek chaczapuri') ||
          data.hotStarters.includes('Sajgonki') ||
          data.hotStarters.includes('Pierożki wonton') ||
          data.hotStarters.includes('Quiche lorraine') ||
          data.hotStarters.includes('Tortilla z mięsem mielonym') ||
          data.hotStarters.includes('Mule w sosie pomidorowym') ||
          data.hotStarters.includes('Kalmary smażone') ||
          data.hotStarters.includes('Calzone') ||
          data.hotStarters.includes('Bigos') ||
          data.hotStarters.includes('Sycylijska caponata') ||
          // !data.salads2.includes("Sałatka jarzynowa") ||
          // !data.salads2.includes("Sałatka z pieczonego buraka z serem kozim") ||
          data.salads.length !== 2 ||
          data.salads.includes('Sałatka caprese') ||
          data.salads.includes('Sałatka orientalna z krewetkami') ||
          data.salads.includes('Sałatka gyros') ||
          data.salads.includes('Sałatka z grillowanym łososiem') ||
          data.salads.includes('Sałatka z serem kozim') ||
          // !data.additives2.includes("Pieczywo jasne") ||
          // !data.additives2.includes("Pieczywo ciemne") ||
          // !data.additives2.includes("Ogórki kiszone") ||
          // !data.additives2.includes("Grzybki marynowane") ||
          // !data.additives2.includes("Dynia w zalewie") ||
          data.additives.length < 1 ||
          data.additives.includes('Ryż') ||
          data.additives.includes('Chipsy') ||
          data.additives.includes('Kluski śląskie') ||
          data.additives.includes('Grissini') ||
          data.additives.includes('Frytki') ||
          data.additives.includes('Ziemniaki gotowane') ||
          (data.additives.includes('Makaron smażony') &&
            (!data.aperitif.includes('Szampan') ||
              !data.aperitif.includes('Wino musujące') ||
              !data.aperitif.includes('Gin z tonikiem') ||
              !data.aperitif.includes('Wermut z sokiem pomarańczowym') ||
              !data.aperitif.includes('Tonik z plasterkiem cytryny') ||
              !data.aperitif.includes('Sok pomarańczowy ')) &&
            (!data.hotStarters.includes('Grillowane warzywa z oliwą') ||
              !data.hotStarters.includes('Krokiety z mięsem') ||
              !data.hotStarters.includes('Pierogi z grzybami') ||
              !data.hotStarters.includes('Śliwki zawijane w boczku') ||
              !data.hotStarters.includes('Jajka faszerowane')) &&
            (!data.salads.includes('Sałatka jarzynowa') ||
              !data.salads.includes(
                'Sałatka z pieczonego buraka z serem kozim'
              )) &&
            (!data.additives.includes('Pieczywo jasne') ||
              !data.additives.includes('Pieczywo ciemne') ||
              !data.additives.includes('Ogórki kiszone') ||
              !data.additives.includes('Grzybki marynowane') ||
              !data.additives.includes('Dynia w zalewie')))
        ) {
          setIsDinnerEasy(false);
          initModal({
            type: 'error',
            title: 'Niestety...',
            text: t('error3'),
          });
        } else {
          setIsDinnerEasy(true);
          initModal({
            type: 'success',
            title: 'Gratulacje!',
            text: t(
              'Pierwszy poziom został wykonany poprawnie. Zarówno 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatki oraz różne dodatki na uroczystą kolację dla 12 dorosłych osób w tradycyjnej polskiej restauracji w piątkowy wieczór zostały prawidłowo skomponowane. Doskonale poradziłeś sobie z tym zadaniem.'
            ),
          });
        }
      }
    } else if (level === 'veryHard') {
      if (
        soupsNames.length === 0 &&
        mainCourse2Names.length === 0 &&
        desserts2Names.length === 0 &&
        coldDrinks2Names.length === 0 &&
        hotDrinksNames.length === 0 &&
        alcoDrinksNames.length === 0
      ) {
        initModal({
          type: 'warning',
          title: 'Niestety...',
          // title: warningInfo,
          text: t(
            'Niestety, nie wszystko poszło dobrze. Główna część menu (pierwsze danie, dania główne, desery oraz napoje) na uroczystą kolację w tradycyjnej polskiej restauracji jest niekompletne. Zastanów się czy na pewno wybrałeś wszystkie elementy menu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po  kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy.'
          ),
        });
      } else {
        if (
          !data.soups.includes(
            'Barszcz czerwony z uszkami z nadzieniem z borowików'
          ) ||
          data.soups.includes('Gazpacho z jogurtem') ||
          data.soups.includes('Zupa wonton z makaronem ryżowym') ||
          !data.mainCourse.includes('Kaczka z jabłkami i sosem żurawinowym') ||
          !data.mainCourse.includes('Gulasz z dzika z plasterkami marchewki') ||
          !data.mainCourse.includes(
            'Placek ziemniaczany z gulaszem z warzyw'
          ) ||
          data.mainCourse.includes('Chiński smażony ryż z warzywami') ||
          data.mainCourse.includes('Tofu z warzywami z woka') ||
          data.mainCourse.includes('Musaka') ||
          data.mainCourse.includes('Gnocchi z sosem serowym') ||
          data.mainCourse.includes('Ravioli z borowikami') ||
          data.mainCourse.includes('Pizza') ||
          data.mainCourse.includes('Grillowana ośmiornica') ||
          data.mainCourse.includes('Pielmieni ze szpinakiem') ||
          !data.desserts.includes('Szarlotka z lodami') ||
          !data.desserts.includes('Eklerka z czekoladą') ||
          data.desserts.includes('Tiramisu') ||
          data.desserts.includes('Creme brulèe') ||
          data.desserts.includes('Kutia') ||
          data.desserts.includes('Krem bawarski') ||
          data.desserts.includes('Smażone banany w cieście') ||
          // !data.coldDrinks2.includes("Woda gazowana") ||
          // !data.coldDrinks2.includes("Woda niegazowana") ||
          // !data.coldDrinks2.includes("Sok pomarańczowy") ||
          data.coldDrinks.length < 1 ||
          data.coldDrinks.includes('Napój energetyczny') ||
          data.coldDrinks.includes('Napój kolorowy gazowany') ||
          data.coldDrinks.includes('Herbata') ||
          data.salads.length > 0 ||
          data.additives.length > 0 ||
          data.hotStarters.length > 0 ||
          data.coldStarters.length > 0 ||
          data.cake.length > 0 ||
          data.aperitif.length > 0 ||
          // !data.hotDrinks.includes("Espresso") ||
          // !data.hotDrinks.includes("Herbata czarna") ||
          // !data.hotDrinks.includes("Herbata ziołowa") ||
          // !data.hotDrinks.includes("Mięta") ||
          data.hotDrinks.length < 1 ||
          data.hotDrinks.includes('Wódka') ||
          data.hotDrinks.includes('Piwo grzane') ||
          // !data.alcoDrinks.includes("Wino białe wytrawne") ||
          // !data.alcoDrinks.includes("Wino czerwone półwytrawne") ||
          // !data.alcoDrinks.includes("Wódka") ||
          // !data.alcoDrinks.includes("Piwo") ||
          // !data.alcoDrinks.includes("Miód pitny") ||
          data.alcoDrinks.length < 1 ||
          data.alcoDrinks.includes('Ajerkoniak') ||
          data.alcoDrinks.includes('Krem czekoladowo-wiśniowy') ||
          (data.alcoDrinks.includes('Likier cytrynowy') &&
            (!data.coldDrinks.includes('Woda gazowana') ||
              !data.coldDrinks.includes('Woda niegazowana') ||
              !data.coldDrinks.includes('Sok pomarańczowy')) &&
            (!data.hotDrinks.includes('Espresso') ||
              !data.hotDrinks.includes('Herbata czarna') ||
              !data.hotDrinks.includes('Herbata ziołowa') ||
              !data.hotDrinks.includes('Mięta')) &&
            (!data.alcoDrinks.includes('Wino białe wytrawne') ||
              !data.alcoDrinks.includes('Wino czerwone półwytrawne') ||
              !data.alcoDrinks.includes('Wódka') ||
              !data.alcoDrinks.includes('Piwo') ||
              !data.alcoDrinks.includes('Miód pitny')))
        ) {
          setIsDinnerHard(false);
          initModal({
            type: 'error',
            title: 'Niestety...',
            text: t('error4'),
          });
        } else {
          setIsDinnerHard(true);
          initModal({
            type: 'success',
            title: 'Gratulacje!',
            text: t(
              'Ćwiczenie zostało rozwiązane poprawnie. Poszczególne części menu uroczystej kolacji dla 12 dorosłych osób w tradycyjnej polskiej restauracji w piątkowy wieczór, zostały prawidłowo skomponowane. Doskonale poradziłeś sobie z tym zadaniem.'
            ),
          });
        }
      }
    }
  };

  const onClick = () => {
    const newState = {
      ...state,
      [level]: { ...state[level], isValidButtonClicked: true },
    };
    setState(newState);
    if (isValid()) {
      // message.success('Gratulacje!', succesInfo)
      // initModal({ type: "Gratulacje!'", title: "Brawo!", text: successInfo });
    }
  };

  return (
    <>
      <ModalCalculator isOpen={isOpen} setIsOpen={setIsOpen} />
      <div className={classes.wrap}>
        <Tooltip
          title={
            <span style={{ fontSize: '15px' }}>
              Kliknij, aby sprawdzić, czy dokonałeś poprawnego wyboru
            </span>
          }
          placement="top"
          arrow={true}
        >
          <Button
            onClick={onClick}
            variant="contained"
            style={{ marginRight: 10 }}
            color="primary"
          >
            Sprawdź
          </Button>
        </Tooltip>
        <Button
          onClick={() => setIsOpen(true)}
          variant="outlined"
          color="primary"
        >
          Kalkulator ilości dań
        </Button>
      </div>
    </>
  );
};

export default Validation;
