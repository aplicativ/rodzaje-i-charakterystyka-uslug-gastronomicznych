import React, { useContext, useEffect, useState } from "react";
import { Button } from "@material-ui/core";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import DataContex from "../../context/DataContext";
import AudioPlayer from "../AudioPlayer";
import instrukcja from "./../../sounds/instrukcja.mp3";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";

import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import DataContext from "@context/DataContext";

import Select from "react-select";

const useStyles = makeStyles((theme) => ({
  backDropSuccess: {
    background: "rgba(65,172,38,0.3)",
  },
  backDrop: {},
  icon: {
    textAlign: "center",
    marginBottom: "10px",
    width: "100%",
  },
  iconImg: {
    margin: "0 auto",
  },
  title: {
    textAlign: "center",
    position: "relative",
    maxWidth: "100%",
    marginBottom: "20px",
    color: "#595959",
    fontSize: "25px",
    fontWeight: "600",
    textAlign: "center",
    textTransform: "none",
    wordWrap: "break-word",
  },
  description: {
    color: "#595959",
    fontWeight: "600",
    textAlign: "center",
    fontSize: "16px",
    marginBottom: "20px",
  },
  buttonsWrap: {
    display: "flex",
    justifyContent: "center",
  },
  button: {
    margin: "10px",
  },
  paragraph: {
    lineHeight: 1.5,
    marginBottom: 10,
  },
  label: {
    marginBottom: 20,
    // border: "1px solid black",
    borderRadius: 5,
    padding: "10px 0",
  },
  text: {
    marginBottom: 7,
  },
  score: {
    border: 0,
    height: 24,
    width: "100%",
    borderBottom: "2px solid #979797",
  },
  select: {
    maxWidth: "100%",
    width: "100%",
    padding: "3px 0",
    border: 0,
    background: "white",
  },
  suma: {
    backgroundColor: "#1e1c37",
    color: "white",
    padding: "10px",
  },
}));

const Calculator = ({ isOpen, setIsOpen }) => {
  const { level } = useContext(DataContex);
  const classes = useStyles();
  const [type, setType] = useState("porcja");
  const [quantity, setQuantity] = useState(null);
  const [number, setNumber] = useState(null);
  const [score, setScore] = useState(null);

  const { isFlavourTable, setIsFlavourTable, setIsKnowledge } =
    useContext(DataContext);

  const handleClose = () => {
    setIsOpen(false);
  };

  useEffect(() => {
    if (quantity && number && type) {
      setScore(parseInt(quantity) * parseInt(number));
    } else {
      setScore(null);
    }
  }, [type, quantity, number]);

  const renderQuantity = () => {
    if (score) {
      if (type.value === "lyzeczki") {
        return `${score === 1 ? "łyżeczka" : "łyżeczki"}`;
      } else if (type.value === "kostki") {
        return `${score === 1 ? "kostka" : "kostek"}`;
      } else if (type.value === "sztuki") {
        return `${score === 1 ? "sztuka" : "sztuki"}`;
      } else if (type.value === "fruit") {
        return `${score === 1 ? "sztuka" : "sztuki"}`;
      } else if (type.value === "ml") {
        return `ml`;
      } else if (type.value === "lisc") {
        return `${score === 1 ? "liść" : "liście"}`;
      }
    }
  };

  const options = [
    { value: "porcja", label: "porcja" },
    { value: "sztuka", label: "sztuka" },
  ];

  const onChangeSelect = (value) => {
    setType(value);
  };

  return (
    <>
      <Dialog
        fullWidth={true}
        maxWidth={"xs"}
        open={isOpen}
        onClose={handleClose}
        style={{ zIndex: "999999" }}
      >
        <DialogContent style={{ padding: "45px 45px 45px 45px" }}>
          <h3 style={{ marginBottom: "20px" }}>Kalkulator ilości dań</h3>
          <div style={{ position: "absolute", top: "15px", right: "15px" }}>
            <IconButton onClick={() => setIsOpen(false)} aria-label="Example">
              <CloseIcon />
            </IconButton>
          </div>

          <div className={classes.label}>
            <div className={classes.text}>Liczba porcji dania</div>
            <div style={{ padding: 0, marginTop: 0 }}>
              <input
                onChange={(e) => setQuantity(e.target.value)}
                style={{
                  border: 0,
                  height: 33,
                  backgroundColor: "hsl(0, 0%, 100%)",
                  borderColor: "hsl(0, 0%, 80%)",
                  borderRadius: "4px",
                  borderStyle: "solid",
                  borderWidth: "1px",
                  width: "100%",
                  padding: "19px 8px",
                }}
                type="number"
              />
            </div>
          </div>
          <div className={classes.label}>
            <div className={classes.text}>
              Liczba osób uczestniczących w przyjęciu
            </div>
            <div style={{ padding: 0, marginTop: 0 }}>
              <input
                onChange={(e) => setNumber(e.target.value)}
                style={{
                  border: 0,
                  height: 33,
                  backgroundColor: "hsl(0, 0%, 100%)",
                  borderColor: "hsl(0, 0%, 80%)",
                  borderRadius: "4px",
                  borderStyle: "solid",
                  borderWidth: "1px",
                  width: "100%",
                  padding: "19px 8px",
                }}
                type="number"
              />
            </div>
          </div>

          <div className={classes.label}>
            <div className={classes.text}>Jednostka miary</div>
            <div
              style={{
                padding: 0,
                marginTop: 0,
              }}
            >
              <Select
                value={type}
                onChange={onChangeSelect}
                options={options}
                placeholder={"wybierz..."}
              />
            </div>
          </div>

          <div className={`${classes.label} ${classes.suma}`}>
            <div className={classes.text}>Suma:</div>
            <div className={classes.score}>{score ? score : ""}</div>
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default Calculator;
