
import React, { useContext } from "react";
import RootRef from "@material-ui/core/RootRef";
import { Droppable } from "react-beautiful-dnd";
import DataContext from "@context/DataContext";

import List from "@material-ui/core/List";
import ListItem from "./list";
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    list: {
        paddingInlineStart: "0", 
        backgroundColor: "#f4f4f4", 
        minHeight: "30px",
        width:"300px", 
        padding: "0"
    }
}))

const Item = ({ keyData }) => {
    const { columns } = useContext(DataContext);
    const { [keyData]: data } = columns;
    const classes = useStyles();
    return (
        <Droppable droppableId={data.id}>
            {(provided) => (
                <RootRef rootRef={provided.innerRef}>
                    <List className={classes.list}>
                        {data.list.map((itemObject, index) => {
                            return <ListItem index={index} itemObject={itemObject} />;
                        })}
                        {provided.placeholder}
                    </List>
                </RootRef>
            )}
        </Droppable>
    )
}

export default Item;