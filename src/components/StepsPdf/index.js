import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";

import DataContext from "../../context/DataContext";
import { menuItems } from "@data/features";

import "./style.css";

const useStyles = makeStyles((theme) => ({
  wrap: {
    fontSize: "30px",
    padding: "10px",
    // position:"absolute",
    // top:"0",
    // left:"0",
    // zIndex:-1
  },
  label: {
    margin: "20px 0 40px 0",
    paddingBottom: "10px",
    borderBottom: "3px solid #1e1c37",
    fontSize: "40px",
  },
  message: {
    color: "white",
    borderRadius: "10px",
    textAlign: "center",
    padding: "5px",
    position: "relative",
    top: "-30px",
  },
  success: {
    backgroundColor: "#28a745",
  },
  error: {
    backgroundColor: "#dc3545",
  },
  typeOfParty: {
    borderRadius: "6px",
    color: "#000",
    textAlign: "center",
  },
}));

const Steps = ({ type }) => {
  const classes = useStyles();
  const { state, isPartyEasy, isPartyHard, isDinnerEasy, isDinnerHard } =
    useContext(DataContext);

  return (
    <>
      <div style={{ display: "none", width: "297mm" }} id="pdf-file">
        <div className={classes.wrap}>
          <div className={classes.typeOfParty}>
            <h2>Impreza urodzinowa 18-nastka</h2>
          </div>
          <div className={classes.label}>Poziom łatwy:</div>
          <table id="table-steps">
            {menuItems
              .filter((item) => state.easy[item.keyData].length > 0)
              .map((item) => (
                <tr>
                  <td>{item.name}</td>
                  <td>
                    <ul>
                      {state.easy[item.keyData].map((item) => (
                        <li>{item}</li>
                      ))}
                    </ul>
                  </td>
                </tr>
              ))}
          </table>
          {isPartyEasy ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane poprawnie
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}
          <div className={classes.label}>Poziom trudny:</div>
          <table id="table-steps">
            {menuItems
              .filter((item) => state.average[item.keyData].length > 0)
              .map((item) => (
                <tr>
                  <td>{item.name}</td>
                  <td>
                    <ul>
                      {state.average[item.keyData].map((item) => (
                        <li>{item}</li>
                      ))}
                    </ul>
                  </td>
                </tr>
              ))}
          </table>
          {isPartyHard ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane poprawnie
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}
          <div className={classes.typeOfParty}>
            <h2>Uroczysta kolacja</h2>
          </div>
          <div className={classes.label}>Poziom łatwy:</div>
          <table id="table-steps">
            {menuItems
              .filter((item) => state.hard[item.keyData].length > 0)
              .map((item) => (
                <tr>
                  <td>{item.name}</td>
                  <td>
                    <ul>
                      {state.hard[item.keyData].map((item) => (
                        <li>{item}</li>
                      ))}
                    </ul>
                  </td>
                </tr>
              ))}
          </table>
          {isDinnerEasy ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane poprawnie
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}
          <div className={classes.label}>Poziom trudny:</div>
          <table id="table-steps">
            {menuItems
              .filter((item) => state.veryHard[item.keyData].length > 0)
              .map((item) => (
                <tr>
                  <td>{item.name}</td>
                  <td>
                    <ul>
                      {state.veryHard[item.keyData].map((item) => (
                        <li>{item}</li>
                      ))}
                    </ul>
                  </td>
                </tr>
              ))}
          </table>
          {isDinnerHard ? (
            <div className={`${classes.message} ${classes.success}`}>
              Zadanie wykonane poprawnie
            </div>
          ) : (
            <div className={`${classes.message} ${classes.error}`}>
              Zadanie wykonane nieprawidłowo
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Steps;
