import { makeStyles } from "@material-ui/core/styles";



const useStyles = makeStyles((theme) => ({
    modal: {
        position: 'fixed',
        bottom: "40px",
        left: "50%",
        width: "90%",
        maxWidth: "800px",
        transform: "translateX(-50%)",
        zIndex: "999999" 
    },
    animatedItem: {
        animation: `$myEffect 1500ms ${theme.transitions.easing.easeInOut}`
    },
    animatedItemExiting: {
        animation: `$myEffectExit 1000ms ${theme.transitions.easing.easeInOut}`,
        opacity: 0,
        transform: "translateY(200%) translateX(-50%)"
    },
    "@keyframes myEffect": {
        "0%": {
            opacity: 0,
            transform: "translateY(200%) translateX(-50%)"
        },
        "100%": {
            opacity: 1,
            transform: "translateY(0) translateX(-50%)"
        }
    },
    "@keyframes myEffectExit": {
        "0%": {
            opacity: 1,
            transform: "translateY(0) translateX(-50%)"
        },
        "100%": {
            opacity: 0,
            transform: "translateY(200%) translateX(-50%)"
        }
    }
}));


export default useStyles;