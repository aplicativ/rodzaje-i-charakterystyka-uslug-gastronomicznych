// import React, { useContext } from "react";
// import clsx from "clsx";
// import AppBar from "@material-ui/core/AppBar";
// import Toolbar from "@material-ui/core/Toolbar";
// import IconButton from "@material-ui/core/IconButton";
// import MenuIcon from "@material-ui/icons/Menu";
// import Tabs from "@material-ui/core/Tabs";
// import Tab from "@material-ui/core/Tab";

// import DataContex from "@context/DataContext";
// import useStyles from "./styles";

// import FullscreenIn from "@material-ui/icons/Fullscreen";
// import FullscreenExit from "@material-ui/icons/FullscreenExit";

// const tabProps = (index) => {
//   return {
//     id: `simple-tab-${index}`,
//     "aria-controls": `simple-tabpanel-${index}`,
//   };
// };

// const Header = ({ isOpenNavigation, setIsOpenNavigation }) => {
//   const classes = useStyles();
//   const { setState, level, state, isFullscreen, setIsFullscreen } =
//     useContext(DataContex);
//   const handleLevelChange = (event, newValue) => {
//     setState({ ...state, level: newValue });
//   };
//   const handleChangeFullscreen = () => {
//     setIsFullscreen(!isFullscreen);
//   };

//   return (
//     <AppBar
//       position="fixed"
//       className={clsx(classes.appBar, {
//         [classes.appBarShift]: isOpenNavigation,
//       })}
//     >
//       <Toolbar className={classes.toolbar}>
//         <Tabs value={level} onChange={handleLevelChange} aria-label="">
//           <div style={{ backgroundColor: "#1491d0" }}>
//             <p style={{ textAlign: "center" }}>Impreza urodzinowa 18-stka</p>
//             <Tab
//               className={classes.menuItem}
//               value="easy"
//               label="Łatwy"
//               {...tabProps(0)}
//             />
//             <Tab
//               className={classes.menuItem}
//               value="trudny"
//               label="Trudny"
//               {...tabProps(1)}
//             />
//           </div>{" "}
//           <div style={{ backgroundColor: "#ffc445" }}>
//             <p style={{ textAlign: "center" }}>Uroczysta kolacja</p>
//             <Tab
//               className={classes.menuItem}
//               value="Łatwy2"
//               label="Łatwy"
//               {...tabProps(3)}
//             />
//             <Tab
//               className={classes.menuItem}
//               value="Trudny2"
//               label="Średni"
//               {...tabProps(4)}
//             />
//           </div>
//         </Tabs>
//       </Toolbar>
//       <IconButton
//         color="inherit"
//         aria-label="open drawer"
//         onClick={() => setIsOpenNavigation(!isOpenNavigation)}
//         edge="start"
//         className={clsx(
//           classes.menuButtonMenu,
//           isOpenNavigation && classes.hide
//         )}
//       >
//         <MenuIcon />
//       </IconButton>
//       <IconButton
//         color="inherit"
//         aria-label="open drawer"
//         onClick={handleChangeFullscreen}
//         edge="start"
//         className={clsx(classes.menuButtonFullscreen)}
//       >
//         {isFullscreen ? <FullscreenExit /> : <FullscreenIn />}
//       </IconButton>
//     </AppBar>
//   );
// };

// export default Header;
import React, { useContext } from "react";
import clsx from "clsx";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import FullscreenIn from "@material-ui/icons/Fullscreen";

import FullscreenExit from "@material-ui/icons/FullscreenExit";
import CustomTooltip from "@components/CustomTooltip";

import { useTranslation } from "react-i18next";

import DataContex from "@context/DataContext";
import useStyles from "./styles";
import Tooltip from "@material-ui/core/Tooltip";

const tabProps = (index) => {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
};

const Header = ({ isOpenNavigation, setIsOpenNavigation }) => {
  const { t, i18n } = useTranslation("common");
  const classes = useStyles();
  const { setState, level, state, isFullscreen, setIsFullscreen } =
    useContext(DataContex);

  const handleLevelChange = (event, newValue) => {
    setState({ ...state, level: newValue });
  };
  const handleChangeFullscreen = () => {
    setIsFullscreen(!isFullscreen);
  };

  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: isOpenNavigation,
      })}
    >
      <Toolbar className={classes.toolbar}>
        <Tabs value={level} onChange={handleLevelChange} aria-label="">
          <Tab
            className={classes.menuItem}
            value="easy"
            label={t("easy")}
            {...tabProps(0)}
          />
          <Tab
            className={classes.menuItem}
            value="average"
            label={t("average")}
            {...tabProps(1)}
          />
          <Tab
            className={classes.menuItem}
            value="hard"
            label={
              <div
                dangerouslySetInnerHTML={{
                  __html: t("hard"),
                }}
              />
            }
            {...tabProps(2)}
          />
          <Tab
            className={classes.menuItem}
            value="veryHard"
            label={
              <div
                dangerouslySetInnerHTML={{
                  __html: t("veryHard"),
                }}
              />
            }
            {...tabProps(3)}
          />
        </Tabs>
      </Toolbar>
      <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={() => setIsOpenNavigation(!isOpenNavigation)}
        edge="start"
        className={clsx(
          classes.menuButtonMenu,
          isOpenNavigation && classes.hide
        )}
      >
        <MenuIcon />
      </IconButton>

      <Tooltip
        placement="bottom"
        arrow={true}
        title={
          isFullscreen ? (
            <span style={{ fontSize: "12px" }}>Wyłącz tryb pełnoekranowy</span>
          ) : (
            <span style={{ fontSize: "12px" }}>Włącz tryb pełnoekranowy</span>
          )
        }
      >
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleChangeFullscreen}
          edge="start"
          className={clsx(classes.menuButtonFullscreen)}
        >
          {isFullscreen ? <FullscreenExit /> : <FullscreenIn />}
        </IconButton>
      </Tooltip>
    </AppBar>
  );
};

export default Header;
