const aliases = (prefix = `src`) => ({
    '@components': `${prefix}/components/`,
    '@data': `${prefix}/data/`,
    '@lib': `${prefix}/lib/`,
    '@context': `${prefix}/context/`
  });
  
  module.exports = aliases;