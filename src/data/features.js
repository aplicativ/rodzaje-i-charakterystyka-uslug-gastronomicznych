const typesData = [
  { key: 'woman', name: 'odzież damska', level: ['easy', 'hard'] },
  { key: 'man', name: 'odzież męska', level: ['all'] },
  { key: 'woman-lingerie', name: 'bielizna damska', level: ['average'] },
  { key: 'decoration', name: 'wyrób dekoracji wnętrz', level: ['hard'] },
];

const coldStarters = [
  {
    key: 'Dolmadakia - greckie gołąbki',
    name: 'Dolmadakia - greckie gołąbki',
    level: ['easy'],
  },
  {
    key: 'Deska serow włoskich',
    name: 'Deska serów włoskich',
    level: ['easy'],
  },
  { key: 'Czarne oliwki', name: 'Czarne oliwki', level: ['easy'] },
  { key: 'Zielone oliwki', name: 'Zielone oliwki', level: ['easy'] },
  {
    key: 'Mozzarella z pomidorami, oliwą i bazylią',
    name: 'Mozzarella z pomidorami, oliwą i bazylią',
    level: ['easy'],
  },
  { key: 'Karp w galarecie', name: 'Karp w galarecie', level: ['easy'] },

  { key: 'Deska wędlin', name: 'Deska wędlin włoskich', level: ['easy'] },

  { key: 'Wędzona makrela', name: 'Wędzona makrela', level: ['easy'] },

  { key: 'Śledzie w śmietanie', name: 'Śledzie w śmietanie', level: ['easy'] },
  {
    key: 'Śledzie w sosie curry',
    name: 'Śledzie w sosie curry',
    level: ['easy'],
  },
  { key: 'Indyk w galarecie', name: 'Indyk w galarecie', level: ['easy'] },
  { key: 'Ryba po grecku', name: 'Ryba po grecku', level: ['easy'] },
  {
    key: 'Deska wędlin',
    name: 'Deska wędlin',
    level: ['easy'],
  },

  { key: 'Tatar wołowy', name: 'Tatar wołowy', level: ['easy'] },
  { key: 'Carpaccio', name: 'Carpaccio', level: ['easy'] },

  { key: 'Bruschetta', name: 'Bruschetta', level: ['easy'] },
  {
    key: 'Deska serów polskich',
    name: 'Deska serów polskich',
    level: ['easy'],
  },
  {
    key: 'Placki warzywne z cukinią i wędzonym pstrągiem',
    name: 'Placki warzywne z cukinią i wędzonym pstrągiem',
    level: ['easy'],
  },
  { key: 'Szynka z melonem', name: 'Szynka z melonem', level: ['easy'] },
];
const hotStarters = [
  { key: 'Kalmary smażone', name: 'Kalmary smażone', level: ['easy'] },
  { key: 'Sycylijska caponata', name: 'Sycylijska caponata', level: ['easy'] },
  { key: 'Placki ziemniaczane', name: 'Placki ziemniaczane', level: ['easy'] },

  { key: 'Pierożki wonton', name: 'Pierożki wonton', level: ['easy'] },

  {
    key: 'Grillowane warzywa z oliwą',
    name: 'Grillowane warzywa z oliwą',
    level: ['easy'],
  },

  { key: 'Sajgonki', name: 'Sajgonki', level: ['easy'] },
  { key: 'Bigos', name: 'Bigos', level: ['easy'] },
  {
    key: 'Mule w sosie pomidorowym',
    name: 'Mule w sosie pomidorowym',
    level: ['easy'],
  },
  { key: 'Quiche lorraine', name: 'Quiche lorraine', level: ['easy'] },

  {
    key: 'Krokiety z pieczarkami',
    name: 'Krokiety z pieczarkami',
    level: ['easy'],
  },
  { key: 'Calzone', name: 'Calzone', level: ['easy'] },
  { key: 'Krokiety z mięsem', name: 'Krokiety z mięsem', level: ['easy'] },
  { key: 'Placek chaczapuri', name: 'Placek chaczapuri', level: ['easy'] },

  {
    key: 'Śliwki zawijane w boczku',
    name: 'Śliwki zawijane w boczku',
    level: ['easy'],
  },
  { key: 'Jajka faszerowane', name: 'Jajka faszerowane', level: ['easy'] },
  {
    key: 'Tortilla z mięsem mielonym',
    name: 'Tortilla z mięsem mielonym',
    level: ['easy'],
  },
  { key: 'Pierogi z grzybami', name: 'Pierogi z grzybami', level: ['easy'] },
];
const salads = [
  {
    key: 'Sałatka z serem kozim',
    name: 'Sałatka z serem kozim',
    level: ['easy'],
  },

  {
    key: 'Sałatka caprese',
    name: 'Sałatka caprese',
    level: ['easy'],
  },

  {
    key: 'Sałatka z oscypkiem',
    name: 'Sałatka z oscypkiem',
    level: ['easy'],
  },
  {
    key: 'Sałatka z grillowanym łososiem',
    name: 'Sałatka z grillowanym łososiem',
    level: ['easy'],
  },
  {
    key: 'Sałatka jarzynowa',
    name: 'Sałatka jarzynowa',
    level: ['easy'],
  },
  {
    key: 'Sałatka z kaszą gryczaną i warzywami',
    name: 'Sałatka z kaszą gryczaną i warzywami',
    level: ['easy'],
  },

  {
    key: 'Sałatka orientalna z krewetkami',
    name: 'Sałatka orientalna z krewetkami',
    level: ['easy'],
  },
  { key: 'Sałatka gyros', name: 'Sałatka gyros', level: ['easy'] },
  {
    key: 'Sałatka z pieczonego buraka z serem kozim',
    name: 'Sałatka z pieczonego buraka z serem kozim',
    level: ['easy'],
  },
];
const additives = [
  { key: 'Chipsy', name: 'Chipsy', level: ['easy'] },
  { key: 'Kluski śląskie', name: 'Kluski śląskie', level: ['easy'] },
  { key: 'Grissini', name: 'Grissini', level: ['easy'] },
  { key: 'Pieczywo jasne', name: 'Pieczywo jasne', level: ['easy'] },
  { key: 'Ziemniaki gotowane', name: 'Ziemniaki gotowane', level: ['easy'] },
  { key: 'Pieczywo ciemne', name: 'Pieczywo ciemne', level: ['easy'] },
  { key: 'Makaron smażony', name: 'Makaron smażony', level: ['easy'] },
  { key: 'Dynia w zalewie', name: 'Dynia w zalewie', level: ['easy'] },
  { key: 'Ryż', name: 'Ryż', level: ['easy'] },
  { key: 'Grzybki marynowane', name: 'Grzybki marynowane', level: ['easy'] },
  { key: 'Frytki', name: 'Frytki', level: ['easy'] },
  { key: 'Ogórki kiszone', name: 'Ogórki kiszone', level: ['easy'] },
];

const mainCourse = [
  { key: 'Pierogi z jagodami', name: 'Pierogi z jagodami', level: ['hard'] },
  {
    key: 'Wołowina po burgundzku',
    name: 'Wołowina po burgundzku',
    level: ['hard'],
  },
  { key: 'Mandaryńskie placki', name: 'Mandaryńskie placki', level: ['hard'] },

  { key: 'Musaka', name: 'Musaka', level: ['hard'] },
  {
    key: 'Gnocchi z sosem serowym',
    name: 'Gnocchi z sosem serowym',
    level: ['hard'],
  },

  {
    key: 'Tagiatelle alla carbonara',
    name: 'Tagiatelle alla carbonara',
    level: ['hard'],
  },
  {
    key: 'Ravioli z borowikami',
    name: 'Ravioli z borowikami',
    level: ['hard'],
  },

  {
    key: 'Penne z kurczakiem i szpinakiem',
    name: 'Penne z kurczakiem i szpinakiem',
    level: ['hard'],
  },
  {
    key: 'Polędwiczki wieprzowe z ziemniakami',
    name: 'Polędwiczki wieprzowe z ziemniakami',
    level: ['hard'],
  },
  { key: 'Gyros z jagnięciny', name: 'Gyros z jagnięciny', level: ['hard'] },

  { key: 'Barszcz', name: 'Barszcz', level: ['hard'] },
  { key: 'Pizza Margherita', name: 'Pizza Margherita', level: ['hard'] },

  { key: 'Chłodnik litewski', name: 'Chłodnik litewski', level: ['hard'] },
  {
    key: 'Chiński smażony ryż z warzywami',
    name: 'Chiński smażony ryż z warzywami',
    level: ['hard'],
  },
  {
    key: 'Placek ziemniaczany z gulaszem z warzyw',
    name: 'Placek ziemniaczany z gulaszem z warzyw',
    level: ['hard'],
  },
  {
    key: 'Grillowana ośmiornica',
    name: 'Grillowana ośmiornica',
    level: ['hard'],
  },
  {
    key: 'Tofu z warzywami z woka',
    name: 'Tofu z warzywami z woka',
    level: ['hard'],
  },
  {
    key: 'Kaczka z jabłkami i sosem żurawinowym',
    name: 'Kaczka z jabłkami i sosem żurawinowym',
    level: ['hard'],
  },
  {
    key: 'Gulasz z dzika z plasterkami marchewki',
    name: 'Gulasz z dzika z plasterkami marchewki',
    level: ['hard'],
  },
  {
    key: 'Pielmieni ze szpinakiem',
    name: 'Pielmieni ze szpinakiem',
    level: ['hard'],
  },
];
const desserts = [
  { key: 'Kutia', name: 'Kutia', level: ['hard'] },
  { key: 'Tiramisu', name: 'Tiramisu', level: ['hard'] },
  { key: 'Panna cotta', name: 'Panna cotta', level: ['hard'] },
  { key: 'Strudel jabłkowy', name: 'Strudel jabłkowy', level: ['hard'] },
  { key: 'Zupa owocowa', name: 'Zupa owocowa', level: ['hard'] },
  { key: 'Lody cytrynowe', name: 'Lody cytrynowe', level: ['hard'] },
  { key: 'Racuchy', name: 'Racuchy', level: ['hard'] },
  {
    key: 'Krem bawarski',
    name: 'Krem bawarski',
    level: ['hard'],
  },

  {
    key: 'Creme brulèe',
    name: 'Creme brulèe',
    level: ['hard'],
  },

  {
    key: 'Eklerka z czekoladą',
    name: 'Eklerka z czekoladą',
    level: ['hard'],
  },

  {
    key: 'Smażone banany w cieście',
    name: 'Smażone banany w cieście',
    level: ['hard'],
  },
  {
    key: 'Szarlotka z lodami',
    name: 'Szarlotka z lodami',
    level: ['hard'],
  },
];

const coldDrinks = [
  { key: 'Napój energetyczny', name: 'Napój energetyczny', level: ['hard'] },
  { key: 'Woda gazowana', name: 'Woda gazowana', level: ['hard'] },
  { key: 'Herbata', name: 'Herbata', level: ['hard'] },
  { key: 'Sok pomarańczowy', name: 'Sok pomarańczowy', level: ['hard'] },
  { key: 'Woda niegazowana', name: 'Woda niegazowana', level: ['hard'] },
  {
    name: 'Napój kolorowy gazowany',
    key: 'Napój kolorowy gazowany',
    level: ['hard'],
  },
];

const cake = [
  { key: 'Tort a la Sacher', name: 'Tort a la Sacher', level: ['hard'] },
  { key: 'Tort tiramisu', name: 'Tort tiramisu', level: ['hard'] },
  { key: 'Tort makowy', name: 'Tort makowy', level: ['hard'] },
  { key: 'Tort cytrynowy', name: 'Tort cytrynowy', level: ['hard'] },
  { key: 'Tort piernikowy', name: 'Tort piernikowy', level: ['hard'] },
  { key: 'Tort migdałowy', name: 'Tort migdałowy', level: ['hard'] },
];

const aperitif = [
  { key: 'Szampan', name: 'Szampan', level: ['easy'] },

  { key: 'Gin z tonikiem', name: 'Gin z tonikiem', level: ['easy'] },
  {
    key: 'Wermut z sokiem pomarańczowym',
    name: 'Wermut z sokiem pomarańczowym',
    level: ['easy'],
  },
  { key: 'Ajerkoniak', name: 'Ajerkoniak', level: ['easy'] },
  {
    key: 'Tonik z plasterkiem cytryny',
    name: 'Tonik z plasterkiem cytryny',
    level: ['easy'],
  },
  { key: 'Sok pomarańczowy ', name: 'Sok pomarańczowy ', level: ['easy'] },
  { key: 'Limoncello', name: 'Limoncello', level: ['easy'] },
  { key: 'Wino wytrawne', name: 'Wino wytrawne', level: ['easy'] },
  { key: 'Wino musujące', name: 'Wino musujące', level: ['easy'] },
  { key: 'Krem czekoladowy', name: 'Krem czekoladowy', level: ['easy'] },
];

const soups = [
  {
    key: 'Zupa wonton z makaronem ryżowym',
    name: 'Zupa wonton z makaronem ryżowym',
    level: ['hard'],
  },
  {
    key: 'Barszcz czerwony z uszkami z nadzieniem z borowików',
    name: 'Barszcz czerwony z uszkami z nadzieniem z borowików',
    level: ['hard'],
  },
  { key: 'Gazpacho z jogurtem', name: 'Gazpacho z jogurtem', level: ['hard'] },
];

const hotDrinks = [
  {
    key: "Wódka ",
    name: "Wódka ",
    level: ["hard"],
  },
  {
    key: 'Piwo grzane',
    name: 'Piwo grzane',
    level: ['hard'],
  },
  {
    key: 'Espresso',
    name: 'Espresso',
    level: ['hard'],
  },

  {
    key: 'Herbata ziołowa',
    name: 'Herbata ziołowa',
    level: ['hard'],
  },
  {
    key: 'Mięta',
    name: 'Mięta',
    level: ['hard'],
  },
  {
    key: 'Herbata czarna',
    name: 'Herbata czarna',
    level: ['hard'],
  },
];

const alcoDrinks = [
  {
    key: 'Wino białe wytrawne',
    name: 'Wino białe wytrawne',
    level: ['hard'],
  },
  {
    key: 'Miód pitny',
    name: 'Miód pitny',
    level: ['hard'],
  },

  {
    key: 'Wódka',
    name: 'Wódka',
    level: ['hard'],
  },
  {
    key: 'Likier cytrynowy',
    name: 'Likier cytrynowy',
    level: ['hard'],
  },
  {
    key: 'Piwo',
    name: 'Piwo',
    level: ['hard'],
  },

  {
    key: 'Ajerkoniak',
    name: 'Ajerkoniak',
    level: ['hard'],
  },
  {
    key: 'Krem czekoladowo-wiśniowy',
    name: 'Krem czekoladowo-wiśniowy',
    level: ['hard'],
  },
  {
    key: 'Wino czerwone półwytrawne',
    name: 'Wino czerwone półwytrawne',
    level: ['hard'],
  },
];
const fabricsData = [
  {
    title: 'Tkaniny',
    items: [
      { key: 'cotton', name: 'Bawełna', level: ['all'] },
      { key: 'wool', name: 'Wełna', level: ['all'] },
      { key: 'linen', name: 'Len', level: ['easy'] },
      { key: 'polyester', name: 'Poliester', level: ['average'] },
      { key: 'silk', name: 'Jedwab', level: ['easy'] },
      { key: 'velvet', name: 'Aksamit', level: ['all'] },
    ],
  },
  {
    title: 'Dzianiny',
    items: [
      { key: 'jersey', name: 'Dżersej', level: ['all'] },
      { key: 'jersey-front', name: 'Dzianina frotowa', level: ['all'] },
      { key: 'jersey-plush', name: 'Dzianina pluszowa,', level: ['hard'] },
      { key: 'jersey-casual', name: 'Dzianina dresowa ', level: ['average'] },
      { key: 'jersey-jumper', name: 'Dzianina swetrowa ', level: ['hard'] },
    ],
  },
];

const dragAndDropData = {
  menu: {
    id: 'menu',
    list: [
      { key: '1', text: 'Cięcie plecków do formatu' },
      { key: '4', text: 'Wykonanie zawieszenia' },
      { key: '3', text: 'Doklejanie główki (jeżeli nie zintegrowana)' },
      { key: '2', text: 'Doklejanie kalendariów' },
    ],
  },
  content1: {
    id: 'content1',
    list: [{ key: '2', text: 'Doklejanie kalendariów' }],
  },
  content2: {
    id: 'content2',
    list: [
      // { key: "x3", text: "CC1" },
    ],
  },
};

const menuItems = [
  {
    name: 'Dania główne:',
    keyData: 'mainCourse',
  },
  {
    name: 'Dania pierwsze:',
    keyData: 'soups',
  },
  {
    name: 'Desery',
    keyData: 'desserts',
  },
  {
    name: 'Zimne napoje:',
    keyData: 'coldDrinks',
  },
  {
    name: 'Ciepłe napoje:',
    keyData: 'hotDrinks',
  },
  {
    name: 'Napoje alkoholowe:',
    keyData: 'alcoDrinks',
  },
  {
    name: 'Aperitif',
    keyData: 'aperitif',
  },
  {
    name: 'Torty',
    keyData: 'cake',
  },
  {
    name: 'Zimne przystawki:',
    keyData: 'coldStarters',
  },
  {
    name: 'Ciepłe przystawki:',
    keyData: 'hotStarters',
  },
  {
    name: 'Sałatki',
    keyData: 'salads',
  },
  {
    name: 'Dodatki różne:',
    keyData: 'additives',
  },
];

export {
  typesData,
  coldStarters,
  hotStarters,
  desserts,
  hotDrinks,
  salads,
  alcoDrinks,
  coldDrinks,
  mainCourse,
  additives,
  soups,
  aperitif,
  cake,
  fabricsData,
  dragAndDropData,
  menuItems,
};
