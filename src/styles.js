import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    // display: "flex",
    transform: "translate(0,0)",
    background: "white",

  },
  fullScreen: {
    position: "fixed",
    top: "0",
    left: "0",
    width: "100%",
    height: "100%",
    zIndex: "99999",
    // outline: "1px solid blue"
  },

  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    margin: "65px 0 95px 0",
    minHeight: "801px",
    width: "100%",
    position: "relative",
    zIndex: "1",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "10px",
    height: "calc(100vh - 140px)",
    overflowY: "scroll !important",
    transition: theme.transitions.create("padding", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    paddingLeft: 0,
  },
  contentShift: {
    transition: theme.transitions.create("padding", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    paddingLeft: drawerWidth,
  },
  task: {
    width: "100%",
    marginBottom: "80px"
  },
  contentFullscreen: {

  },
  footer: {
    bottom: 0,
    left: "auto",
    width: "100%",
    height: "65px",
    right: 0,
    position: "fixed",
    zIndex: 2,
    transition: theme.transitions.create("padding", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
}));

// const useStyles = makeStyles((theme) => ({
//   root: {
//     // display: "flex",
//     transform: "translate(0,0)",
//     background: "white",
//     // maxWidth: "640px",
//     margin: "auto",
//   },
//   fullScreen: {
//     position: "fixed",
//     top: "0",
//     left: "0",
//     width: "100%",
//     maxWidth: "100%",
//     height: "100%",
//     zIndex: "99999",
//     // outline: "1px solid blue"
//   },

//   drawerHeader: {
//     display: "flex",
//     alignItems: "center",
//     padding: theme.spacing(0, 1),
//     ...theme.mixins.toolbar,
//     justifyContent: "flex-end",
//   },
//   content: {
//     margin: "65px 0 95px 0",
//     minHeight: "801px",
//     width: "100%",
//     position: "relative",
//     zIndex: "1",
//     display: "flex",
//     flexDirection: "column",
//     alignItems: "center",
//     padding: "10px",
//     height: "calc(100vh - 140px)",
//     overflowY: "scroll !important",
//   },
//   task: {
//     width: "100%",
//     marginTop: "35px",
//     marginBottom: "80px",
//   },
//   contentFullscreen: {},

//   footer: {
//     bottom: "0",
//     left: "auto",
//     width: "100%",
//     height: "65px",
//     right: "0",
//     position: "fixed",
//     zIndex: "2",
//   },
// }));

export default useStyles;
