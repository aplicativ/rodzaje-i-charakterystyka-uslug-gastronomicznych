export default {
  common: {
    easy : "The 18th birthday party easy level",
    average: "The 18th birthday party difficult level",
    hard: "<span>Gala dinner <br/> easy level</span>",
    veryHard: "<span>Gala dinner <br/> difficult level</span>",

    "polecenie" : "TASK",

    // Main course
    "Baza dań głównych": "Main courses database",

    "Pierogi z jagodami":"Blueberry dumplings",
    "Wołowina po burgundzku":"Beef burgundy",
    "Mandaryńskie placki":"Mandarin pancakes",
    "Gnocchi z sosem serowym":"Gnocchi with cheese sauce",
    "Tagiatelle alla carbonara":"Tagiatelle alla carbonara",
    "Ravioli z borowikami":"Ravioli with boletus mushrooms",
    "Penne z kurczakiem i szpinakiem":"Penne with chicken and spinach",
    "Tofu z warzywami z woka":"Tofu with vegetables from the wok",
    "Polędwiczki wieprzowe z ziemniakami":"Pork tenderloin with potatoes",
    "Gyros z jagnięciny":"Lamb gyros",
    "Barszcz":"Borscht",
    "Pizza Margherita":"Pizza Margherita",
    "Chłodnik litewski":"Lithuanian Cold Soup",
    "Chiński smażony ryż z warzywami":"Chinese fried rice with vegetables",
    "Placek ziemniaczany z gulaszem z warzyw": "Potato pancake with vegetable stew",
    "Grillowana ośmiornica":"Grilled octopus",
    "Musaka":"Musaka",
    "Kaczka z jabłkami i sosem żurawinowym":"Duck with apples and cranberry sauce",
    "Gulasz z dzika z plasterkami marchewki": "Wild boar stew with sliced carrots",
    "Pielmieni ze szpinakiem":"Dumplings with spinach",

    // First course
    "Baza dań pierwszych": "First course base",

    "Zupa wonton z makaronem ryżowym": "Wonton soup with rice noodles",
    "Barszcz czerwony z uszkami z nadzieniem z borowików":
      "Red borscht with ravioli stuffed with boletus mushrooms",
    "Gazpacho z jogurtem": "Gazpacho with yoghur",

    // Desserts
    "Baza deserów": "Dessert base",

    "Kutia": "Kutia",
    "Tiramisu": "Tiramisu",
    "Panna cotta":"Panna cotta",
    "Strudel jabłkowy":"Apple strudel",
    "Zupa owocowa":"Fruit soup",
    "Lody cytrynowe":"Lemon ice cream",
    "Racuchy":"Crumpets",
    "Krem bawarski":"Bavarian cream",
    "Creme brulèe":"Creme brulèe",
    "Eklerka z czekoladą":"Eclair with chocolate",
    "Smażone banany w cieście": "Fried bananas in pastry",
    "Szarlotka z lodami":"Apple pie with ice cream",

    // Cold drinks
    "Baza zimnych napojów": "Cold drink base",

    "Napój energetyczny": "Energy drink",
    "Woda gazowana": "Sparkling water",
    "Herbata": "Tea",
    "Sok pomarańczowy": "Orange juice",
    "Woda niegazowana": "Still water",
    "Napój kolorowy gazowany": "Carbonated coloured drink",

    // Hot drinks
    "Baza ciepłych napojów": "Hot beverage base",

    "Piwo grzane": "Mulled beer",
    "Espresso": "Espresso",
    "Herbata ziołowa": "Herbal tea",
   "Mięta": "Mint",
    "Herbata czarna": "Black tea",

    // Alcohol
    "Baza napojów alkoholowych": "Alcoholic beverage base",

    "Wino białe wytrawne": "Dry white wine",
    "Miód pitny": "Mead",
    "Wódka": "Vodka",
    "Likier cytrynowy": "Lemon liqueur",
    "Piwo": "Beer",
    "Krem czekoladowo-wiśniowy": "Chocolate cherry cream",
    "Wino czerwone półwytrawne": "Semi-dry red wine",

    // Aperitif
    "Baza aperitifów": "Aperitif base",

    "Szampan": "Champagne",
    "Gin z tonikiem": "Gin and tonic",
    "Ziołowe digestivo": "Ziołowe digestivo",
    "Wermut z sokiem pomarańczowym": "Vermouth with orange juice",
    "Ajerkoniak": "Eggnog",
    "Tonik z plasterkiem cytryny": "Tonic with lemon slice",
    "Sok pomarańczowy": "Orange juice",
    "Limoncello": "Limoncello",
    "Wino wytrawne": "Dry wine",
    "Wino musujące": "Sparkling wine",
    "Krem czekoladowy": "Chocolate cream",

    //Cake
    "Baza tortów": "Cake base",

    "Tort a la Sacher": "Cake a la Sacher",
    "Tort tiramisu": "Tiramisu cake",
    "Tort makowy": "Poppyseed cake",
    "Tort cytrynowy": "Lemon cake",
    "Tort piernikowy": "Gingerbread cake",
    "Tort migdałowy": "Almond cake",

    // Cold starters
    "Baza zimnych przystawek": "Cold starters base",

    "Dolmadakia - greckie gołąbki":"Dolmadakia - Greek stuffed cabbage rolls",
    "Szynka z melonem":"Ham with melon",
    "Deska serow włoskich":"Italian cheese board",
    "Czarne oliwki":"Black olives",
    "Zielone oliwki":"Green olives",
    "Mozzarella z pomidorami, oliwą i bazylią":"Mozzarella with tomatoes, oil and basil",
    "Karp w galarecie":"Carp in jelly",
    "Wędzona makrela":"Smoked mackerel",
    "Śledzie w śmietanie":"Herring in cream",
    "Śledzie w sosie curry":"Herring in curry sauce",
    "Indyk w galarecie":"Turkey in jelly",
    "Ryba po grecku":"Fish in Greek style",
    "Deska wędlin":"Platter of cold cuts",
    "Tatar wołowy":"Beef tartare",
    "Carpaccio":"Carpaccio",
    "Bruschetta":"Bruschetta",
    "Deska serów polskich":"Polish cheese board",
    "Placki warzywne z cukinią i wędzonym pstrągiem":"Vegetable pancakes with courgettes and smoked trout",
    "Deska wędlin włoskich":"Italian cold cuts",

    // Hot starters
    "Baza ciepłych przystawek": "Warm appetiser base",

    "Kalmary smażone":"Fried calamari",
    "Sycylijska caponata":"Sicilian caponata",
    "Placki ziemniaczane":"Potato pancakes",
    "Tortilla z mięsem mielonym":"Tortilla with minced meat",
    "Pierożki wonton":"Wonton dumplings",
    "Grillowane warzywa z oliwą":"Grilled vegetables with oil",
    "Placek chaczapuri":"Khachapuri pancakes",
    "Sajgonki":"Saigons",
    "Mule w sosie pomidorowym":"Mules in tomato sauce",
    "Quiche lorraine":"Quiche lorraine",
    "Krokiety z pieczarkami":"Mushroom croquettes",
    "Calzone":"Calzone",
    "Śliwki zawijane w boczku":"Plums wrapped in bacon",
    "Jajka faszerowane":"Stuffed eggs",
    "Pierogi z grzybami":"Dumplings with mushrooms",
    "Krokiety z mięsem":"Croquettes with meat",

    // Salad
    "Baza sałatek": "Salad base",

    "Sałatka z serem kozim":"Salad with goat's cheese",
    "Sałatka caprese":"Caprese salad",
    "Sałatka z oscypkiem":"Salad with oscypek",
    "Sałatka z grillowanym łososiem":"Salad with grilled salmon",
    "Sałatka jarzynowa":"Vegetable salad",
    "Sałatka z kaszą gryczaną i warzywami":"Salad with buckwheat and vegetables",
    "Sałatka orientalna z krewetkami":"Oriental salad with prawns",
    "Sałatka gyros":"Gyros salad",
    "Sałatka z pieczonego buraka z serem kozim":"Roasted beetroot salad with goat's cheese",
    
    // Mixed toppings
    "Dodatki różne": "Mixed toppings",

    "Chipsy":"Chips",
    "Kluski śląskie":"Silesian noodles",
    "Grissini":"Grissini",
    "Pieczywo jasne":"Light bread",
    "Ziemniaki gotowane":"Boiled potatoes",
    "Pieczywo ciemne":"Dark bread",
    "Makaron smażony":"Fried pasta",
    "Dynia w zalewie":"Pumpkin in brine",
    "Ryż":"Rice",
    "Grzybki marynowane":"Pickled mushrooms",
    "Frytki":"French fries",
    "Ogórki kiszone":"Pickled cucumbers",

    // Title 
    "Dania główne:":"Main course:",
    "Dania pierwsze:":"First course:",
    "Desery":"Desserts:",
    "Zimne napoje:":"Cold drinks:",
    "Ciepłe napoje:":"Warm starters:",
    "Napoje alkoholowe:":"Alcoholic beverages:",
    "Aperitif":"Aperitif:",
    "Torty":"Cake:",
    "Zimne przystawki:":"Cold starters:",
    "Ciepłe przystawki:":"Warm starters:",
    "Sałatki":"Salads:",
    "Dodatki różne:":"Miscellaneous side dishes:",

       // Knowledge base
       "Przejdź do filmu edukacyjnego":"Go to the educational video",
       "Przejdź do sekwencji filmowych":"Skip to film sequences",
       "Przejdź do e-booka":"Go to e-book",

       "Kuchnia włoska jest bardzo tradycyjna i różnorodna oraz słynie z wykorzystania świeżych składników (mięsa, ryb czy owoców morza), wielu warzyw (np. pomidorów, cebuli czy czosnku), oliwy z oliwek i przypraw (np. oregano, bazylia, pieprz, tymianek, rozmaryn). Tradycyjny posiłek składa się przystawki, pierwszego dania (np. makaronu lub rzadziej zupy), drugiego dania i deseru. 18-te urodziny to doskonała okazja, aby zorganizować je w formie przyjęcia koktajlowego, gdzie wszystkie wybrane dania będą serwowane na stołach bufetowych. Oto przykładowe kompozycje bufetu złożonego z: 4 zimnych przystawek, 2 ciepłych przystawek, 1 sałatki oraz różnych dodatków. Te dania możesz podać gościom, którzy przybędą świętować 18-te urodziny we włoskiej trattorii." : "Italian cuisine is very traditional and varied and is famous for its use of fresh ingredients (meat, fish or seafood), many vegetables (e.g. tomatoes, onions or garlic), olive oil and spices (e.g. oregano, basil, pepper, thyme, rosemary). A traditional meal consists of a starter, a first course (e.g. pasta or, less frequently, soup), a second course and dessert. An 18th birthday party is the perfect opportunity to organise it as a cocktail party, where all selected dishes will be served on buffet tables. Here are examples of buffet compositions consisting of: 4 cold starters, 2 hot starters, 1 salad and various side dishes. You can serve these dishes to guests who come to celebrate their 18th birthday in an Italian trattoria.",

       "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie, zarówno tacy, którzy przepadają za mięsem oraz tacy, którzy preferują dietę wegetariańską. Warto zwrócić uwagę, aby w zestawie pojawiły się produkty pełnoziarniste, warzywa i ryby." : "With this composition, all guests will find something tasty for themselves, whether they like meat or prefer a vegetarian diet. Make sure to include whole grain products, vegetables and fish.",

       "Dla kuchni włoskiej bardzo charakterystyczne są potrawy mączne (np. makarony, ravioli czy tortellini), które w towarzystwie sosów stają się dość kaloryczne. Dlatego może warto sięgnąć np. po makaron pełnoziarnisty lub wybrać ryby i warzywa.": "Floury dishes (e.g. pasta, ravioli or tortellini) are very characteristic of Italian cuisine and become quite calorific when accompanied by sauces. Therefore, you might want to go for wholemeal pasta, for example, or opt for fish and vegetables.",

       "18-te urodziny to doskonała okazja, aby zorganizować je w formie przyjęcia koktajlowego, gdzie wszystkie wybrane dania będą serwowane na stołach bufetowych. Oto przykładowe kompozycje bufetu złożonego z: 2 dań głównych, deseru, zimnych napojów oraz tortu urodzinowego. Te dania możesz podać gościom, którzy przybędą świętować 18-te urodziny we włoskiej trattorii." :"1An 18th birthday party is the perfect opportunity to organise it as a cocktail party, where all the chosen dishes will be served on buffet tables. Here are examples of buffet compositions consisting of: 2 main courses, dessert, cold drinks and a birthday cake. You can serve these dishes to guests who come to celebrate their 18th birthday in an Italian trattoria.",

       "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie. Miłośnicy mięsa oraz wegetarianie powinni być zadowoleni. Warto zwrócić uwagę, aby nie pojawiały się kolorowe napoje gazowane, można też zastanowić się nad rezygnacją z deseru, skoro na koniec podany zostanie tort urodzinowy. Postarajmy się skomponować różnorodne menu." :"With this composition, all guests will find something tasty for themselves. Meat lovers and vegetarians should be satisfied. Make sure there are no colourful fizzy drinks, and consider omitting dessert if a birthday cake will be served at the end. Let's try to compose a varied menu.",

    "Tradycyjna kuchnia polska oferuje duży repertuar różnie przygotowanych przystawek zimnych i ciepłych (np. śledzie, tatar, jajka faszerowane) oraz rozmaitych dodatków (np. ogórki kiszone, dynia czy grzybki marynowane)." :"Traditional Polish cuisine offers a large repertoire of variously prepared cold and hot starters (e.g. herring, tartar, stuffed eggs) and various side dishes (e.g. pickled cucumbers, pumpkin or pickled mushrooms).",

    "Uroczysta kolacja to doskonała okazja, aby przyjęcie miało charakter zasiadany. Warto zatem pomyśleć o odpowiednio dobranej zastawie i bieliźnie stołowej, a także eleganckiej dekoracji stołów. Goście obsługiwani będą przez kelnerów." :  "A gala dinner is the perfect occasion for a sit-down dinner party. Therefore, it is worth thinking about appropriately selected tableware and linen, as well as elegant table decorations. Guests will be served by waiters.",

    "Oto przykładowa kompozycja bufetu złożonego z: 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatek oraz różnych dodatków. Te dania możesz podać gościom, którzy przybędą na uroczystą kolację do tradycyjnej polskiej restauracji." :  "Here is an example of a buffet composition consisting of: 5 cold starters, 5 hot starters, 2 salads and various side dishes. You can serve these dishes to your guests who will arrive for a gala dinner in a traditional Polish restaurant.",

    "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie, zarówno tacy, którzy przepadają za mięsem oraz tacy, którzy preferują dietę wegetariańską. Warto zwrócić uwagę, aby w zestawie pojawiły się produkty pełnoziarniste, warzywa i ryby." : "With such a composition, all guests will find something tasty for themselves, both those who love meat and those who prefer a vegetarian diet. It is worth ensuring that whole-grain products, vegetables and fish are included.",

    "W tradycyjnej kuchni polskiej dominują potraw z mięsa i różnych warzyw (np. kapusty, ziemniaków), a także ciekawe desery (np. szarlotka, sernik czy pączki). Jej cechą charakterystyczną jest także duża oferta zup." : "Traditional Polish cuisine is dominated by dishes made of meat and various vegetables (e.g. cabbage, potatoes), as well as interesting desserts (e.g. apple pie, cheesecake or doughnuts). Its characteristic feature is also a large offer of soups.",

    "Uroczysta kolacja to doskonała okazja, aby przyjęcie miało charakter zasiadany. Warto zatem pomyśleć o odpowiednio dobranej zastawie i bieliźnie stołowej, a także eleganckiej dekoracji stołów. Goście obsługiwani będą przez kelnerów." :"A gala dinner is the perfect occasion for a sit-down party. Therefore, it is worth thinking about appropriately selected tableware and linen, as well as elegant table decorations. Guests will be served by waiters." ,

    "Oto przykładowa kompozycja menu złożonego z: 1 pierwszego dania, 3 dań głównych do wyboru; 2 deserów do wyboru oraz napojów zimnych, gorących oraz alkoholowych. Te dania możesz podać gościom, którzy przybędą na uroczystą kolację do tradycyjnej polskiej restauracji." : "Here is a sample composition of the menu consisting of: 1 first course, 3 main courses of your choice; 2 desserts of your choice and cold, hot and alcoholic drinks. You can serve these dishes to your guests arriving for a gala dinner in a traditional Polish restaurant.",

    "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie. Miłośnicy mięsa oraz wegetarianie powinni być zadowoleni. Warto zwrócić uwagę, aby nie pojawiały się kolorowe napoje gazowane. Postarajmy się skomponować różnorodne menu." :  "With this composition, all guests will find something tasty for themselves. Meat lovers and vegetarians should be satisfied. Care should be taken to avoid colourful fizzy drinks. Let’s try to compose a varied menu.",

    "Zestaw nr 1" :  "Set No. 1",
    "Zestaw nr 2" :  "Set No. 2",
    "Bufet nr 1" :  "Buffet No. 1",

    // Feedback

    'error1' : "If you are having difficulty with this task, go back to the instruction, and then try to do it again. Make sure that you have chosen not only right cold and hot starters, salads and side dishes indicated on the menu, but also if their number corresponds to the ordered menu. Consider whether all the components of the menu you have selected suit the circumstances and the place, where the birthday party takes place. Perhaps you have chosen dishes that are not typical of Italian cuisine (e.g. herring in cream, khachapuri or salad with oscypek cheese). Next time you will definitely succeed!",

    'error2' : "If you are having difficulty with this task, go back to the instruction, and then try to do it again. Make sure that you have chosen not only right main dishes, deser, cold drinks and a birthday cake indicated on the menu, but also if their number corresponds to the ordered menu. Consider whether all the components of the menu you have selected suit the circumstances and the place, where the birthday party takes place. Perhaps you have chosen dishes that are not typical of Italian cuisine (e.g. borscht, lamb gyros or kutia). Next time you will definitely succeed!",

    'error3' : "If you are having difficulty with this task, go back to the instruction, and then try to do it again. Make sure that you have chosen not only right cold and hot starters, salads and side dishes indicated on the menu, but also if their number corresponds to the ordered menu. Consider whether all the components of the menu you have selected suit the circumstances and the place, where the party takes place. Perhaps you have chosen dishes that are not typical of Polish cuisine (e.g. dolmadakia – Greek stuffed cabbage, spring rolls or Caprese salad). Next time you will definitely succeed!",

    'error4' : "If you are having difficulty with this task, go back to the instruction, and then try to do it again. Make sure that you have chosen not only right first and main dishes, deser, set of cold and hot drinks and alkohol drinks indicated on the menu, but also if their number corresponds to the ordered menu. Consider whether all the components of the menu you have selected suit the circumstances and the place, where the party takes place. Perhaps you have chosen dishes that are not typical of Polish cuisine (e.g. gazpacho with yogurt, Chinese fried rise or Bavarian cream). Next time you will definitely succeed!",

    "Niestety, nie wszystko poszło dobrze. Bufet na imprezę urodzinową we włoskiej trattoria jest niekompletny. Zwróć uwagę czy na pewno wybrałeś wszystkie przystawki zimne, ciepłe, sałatki i dodatki, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy." : " Unfortunately, not everything went well. The buffet for the birthday party at the Italian trattoria is incomplete. Make sure that you have chosen all the cold and hot starters, salads and side dishes indicated on the menu and that the number of starters and salads corresponds to the menu you ordered. If you find the task difficult, keep in mind that in the bottom menu you will also find icons which, by clicking on them, will take you back to the knowledge available in the other multimedia e-materials and to the knowledge base.",

    "Ten poziom został wykonany poprawnie. Doskonale poradziłeś sobie z wyborem 4 zimnych i 2 ciepłych przystawek, 1 sałatki oraz dodatków części menu na imprezę urodzinową dla 8 osób w wieku 16-18 lat oraz 4 osób dorosłych, która odbywa się we włoskiej trattorii. Świetnie!" : "You have completed this level correctly. You have done very well in selecting 4 cold and 2 hot starters, 1 salad and side dishes part of the menu for a birthday party for 8 16-18 year olds and 4 adults held in an Italian trattoria. Great!",

    "Niestety, nie wszystko poszło dobrze. Główna część menu (dania główne, desery, napoje i  tort) na imprezę urodzinową we włoskiej trattoria jest niekompletna. Zastanów się czy na pewno wybrałeś wszystkie elementy menu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy." : " Unfortunately, not everything went well. The main part of the menu (main courses, desserts, drinks and cake) for a birthday party at an Italian trattoria is incomplete. Think about whether you are sure you have chosen all the menu items indicated in the order and whether their number matches the menu you ordered. If you find the task difficult, keep in mind that there are also icons in the lower menu which, by clicking on them, will take you back to the knowledge available in the other multimedia e-materials and to the knowledge base.",


    "Ćwiczenie zostało rozwiązane poprawnie. Poszczególne części menu imprezy urodzinowej dla 8 osób w wieku 16-18 lat oraz 4 osób dorosłych, która odbywa się we włoskiej trattorii, zostały prawidłowo skomponowane. Świetnie!" : "The exercise was solved correctly. The different parts of the menu for a birthday party for 8 16-18 year olds and 4 adults, held in an Italian trattoria, have been composed correctly. Excellent!",

    "Niestety, nie wszystko poszło dobrze. Aperitif oraz bufet na uroczystą kolację w tradycyjnej polskiej restauracji są niekompletne. Zastanów się czy na pewno wybrałeś napoje na aperitif oraz wszystkie elementy bufetu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy." : " Unfortunately, not everything went well. The aperitif and the buffet for a gala dinner in a traditional Polish restaurant are incomplete. Think about whether you have definitely chosen the drinks for the aperitif and all the elements of the buffet indicated in the instruction and whether their number matches the ordered menu. If you find the task difficult, keep in mind that in the bottom menu you will also find icons that you can click on to return to the knowledge available in the other multimedia e-materials and use the knowledge base.",

    "Pierwszy poziom został wykonany poprawnie. Zarówno 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatki oraz różne dodatki na uroczystą kolację dla 12 dorosłych osób w tradycyjnej polskiej restauracji w piątkowy wieczór zostały prawidłowo skomponowane. Doskonale poradziłeś sobie z tym zadaniem." : "The first level was done correctly. Both the 5 cold starters, the 5 hot starters, the 2 salads and the various side dishes for a gala dinner for 12 adults in a traditional Polish restaurant on a Friday evening have been composed correctly. You have coped perfectly with this task.",

    "Niestety, nie wszystko poszło dobrze. Główna część menu (pierwsze danie, dania główne, desery oraz napoje) na uroczystą kolację w tradycyjnej polskiej restauracji jest niekompletne. Zastanów się czy na pewno wybrałeś wszystkie elementy menu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po  kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy." : " Unfortunately, not everything went well. The main part of the menu (first course, main courses, desserts and drinks) for a gala dinner in a traditional Polish restaurant is incomplete. Ask yourself whether you are sure you have chosen all the menu items indicated in the order and whether their number matches the ordered menu. If you find the task difficult, keep in mind that there are also icons in the lower menu which, by clicking on them, will take you back to the knowledge available in the other multimedia e-materials and to the knowledge base.",

    "Ćwiczenie zostało rozwiązane poprawnie. Poszczególne części menu uroczystej kolacji dla 12 dorosłych osób w tradycyjnej polskiej restauracji w piątkowy wieczór, zostały prawidłowo skomponowane. Doskonale poradziłeś sobie z tym zadaniem." :"The exercise was solved correctly. The different parts of the menu for a gala dinner for 12 adults in a traditional Polish restaurant on a Friday evening have been composed correctly. You have dealt with this task perfectly.",

    // Instructions
    'instructionTitle':'Instructions for use for the exercise programme "Arranging menus for special events"',
    "instruction1":"Before starting the exercise, familiarise yourself with the command.",
    "instruction2":"For your convenience in using the exercise programme, click on the full-screen mode icon. It allows the browser to occupy the entire screen.",
    "instruction3" :'To listen to the contents of the exercise programme, select the "Listen" icon. In the bottom menu, you will also find icons which, if you click on them, will allow you to explore other multimedia material from the e-resource.',
    "instruction4": 'If you want to save the progress of your work, use the "Download step list" icon and save the file to your computer.',
    "instruction5" :"The exercise programme also allows you to save all your work, take a screenshot and redo the exercise. Once you have completed the exercise, regardless of your score, you will receive feedback.",
    "instruction6" :"If using the keyboard only, use the following keys:",
    "instructionList": "<ul><li>Tab - moving forward through the elements</li><li>Shift + Tab - move backwards through the items</li><li>Spacebar - lift and drop items</li><li>Escape - cancel dragging</li><li>Arrows - move elements to adjacent drop zones</li></ul>",

    // Recommendation
    // 1
    "Impreza urodzinowa 18-stka" : "The 18th birthday party",

    "Planowana jest impreza z okazji 18 urodzin, w której będzie brać udział 8 osób w wieku 16-18 lat oraz 4 dorosłych. Impreza urodzinowa odbywa się we włoskiej restauracji w sobotę, między godziną 13:00 a 15:00. Jest to przyjęcie bufetowe, które pozwoli gościom na swobodny wybór menu, a także będzie sprzyjało rozmowom, zabawie, czy nawet tańcom." :"The 18th birthday party is being planned, with 8 persons,  16–18-year-olds and 4 adults. The birth-day party takes place in an Italian trattoria on a Saturday, between 1 p.m. and 3 p.m. It is a buffet party, which will allow guests the freedom to choose the menu and will encourage conversation, funor even dancing.",

    "Zamówione urodzinowe menu będzie składało się z następujących elementów:":"The ordered birthday menu will consist of the following:",

    "tzw. bufetu (zimnych i ciepłych przystawek oraz dodatków);" : "the so-called buffet (cold and hot starters and side dishes);",
    "2 dań głównych do wyboru przez gości;" : "2 main courses to be chosen by the guests;",
    "1 deseru;" : "1 dessert;",
    "napojów zimnych;": "cold drinks;",
    "tortu;":" birthday cake;",
    "i wina musującego do wzniesienia toastu urodzinowego." : "and sparkling wine for the birthday toast.",
    "POZIOM ŁATWY": "EASY LEVEL",

    "Twoim zadaniem jest skomponowanie bufetu, który składać się będzie z: 4 zimnych przystawek, 2 ciepłych przystawek, 1 sałatki oraz różnych dodatków. Pamiętaj, że 18-te urodziny odbywają się we włoskiej restauracji. Wybór przystawek powinien być dopasowany do oferty kuchni włoskiej, ale także do wieku gości i okoliczności imprezy. Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu." : "Your task is to compose a buffet consisting of: 4 cold starters, 2 hot starters, 1 salad and various side dishes. Keep in mind that the 18th birthday party takes place in an Italian trattoria. The choice of starters should be tailored to the Italian cuisine on offer, but also to the age of the guests and the circumstances of the party. In addition, correct combinations of dishes with starchy and vitamin supplements, desserts, drinks should be taken into account. All additional information can be found in the lower menu.",
  "4 zimnych przystawek,":'4 cold starters,',
  "2 ciepłych przystawek,":"2 hot starters,",
  "1 sałatki,":"1 salad",
  "oraz różnych dodatków.":"and various side dishes",
  "Pamiętaj, że 18-te urodziny odbywają się we włoskiej restauracji. Wybór przystawek powinien być dopasowany do oferty kuchni włoskiej, ale także do wieku gości i okoliczności imprezy. Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu.":"Keep in mind that the 18th birthday party takes place in an Italian trattoria. The choice of starters should be tailored to the Italian cuisine on offer, but also to the age of the guests and the circumstances of the party. In addition, correct combinations of dishes with starchy and vitamin supplements, desserts, drinks should be taken into account. All additional information can be found in the lower menu.",
  "Twoim zadaniem jest skomponowanie bufetu, który składać się będzie z:":"Your task is to compose a buffet consisting of:",
  // 2
    "Planowana jest impreza z okazji 18 urodzin, w której będzie brać udział 8 osób w wieku 16-18 lat oraz 4 dorosłych. Impreza urodzinowa odbywa się we włoskiej restauracji w sobotę, między godziną 13:00 a 15:00. Jest to przyjęcie bufetowe, które pozwoli gościom na swobodny wybór menu, a także będzie sprzyjało rozmowom, zabawie, czy nawet tańcom." : "The 18th birthday party is being planned, with 8 persons,  16–18-year-olds and 4 adults. The birth-day party takes place in an Italian trattoria on a Saturday, between 1 p.m. and 3 p.m. It is a buffet party, which will allow guests the freedom to choose the menu and will encourage conversation, funor even dancing.",

    "POZIOM TRUDNY": "DIFFICULT LEVEL",

    "Po wybraniu odpowiednich przystawek i dodatków musisz dobrać:":"Once you have selected the appropriate starters and side dishes, you must choose:",
    "2 dania główne":"2 main courses",
    "1 deser":"1 dessert",
    "zimne napoje":'cold drinks',
    "oraz 1 tort.":'and 1 birthday cake.',
    "Pamiętaj, że 18-te urodziny odbywają się we włoskiej restauracji. Wybór dań głównych oraz deseru powinien być dopasowany do repertuaru kuchni włoskiej, ale także do wieku gości i okoliczności imprezy. Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu.":"Keep in mind that the 18th birthday party takes place in an Italian trattoria. The choice of main courses and dessert should be adapted to the repertoire of Italian cuisine, but also to the age of the guests and the circumstances of the party. In addition, the correct combinations of dishes with starch and vitamin supplements, desserts, drinks should be taken into account. You will find all additional information in the lower menu.",
    // 3
    "Uroczysta kolacja" : "The gala dinner",

    "Planowana jest uroczysta kolacja, w której będzie brać udział 12 dorosłych osób. Uroczysta kolacja odbywa się w tradycyjnej polskiej restauracji w piątkowy wieczór.Jest to przyjęcie zasiadane z pełną obsługą kelnerską, czyli zimne i ciepłe przystawki, dania główne, desery i napoje są serwowane gościom na stół.": "Your task is to compose a menu for a gala dinner attended by 12 adults. The gala dinner takes place in a traditional Polish restaurant on a Friday evening. It is a sit-down party with full waiter service, i.e. cold and hot starters, main courses, desserts and drinks are served to the guests at the table",

    "Zamówione menu będzie składało się z następujących elementów:" :"The ordered menu will consist of the following:",

    "zimnych i ciepłych przystawek, sałatek i dodatków;": "cold and hot starters, salads and side dishes;",
    "1 pierwszego dania;":"1 first course;",
    "3 dań głównych do wyboru przez gości;" : "3 main courses of guests' choice;",
    "2 deserów do wyboru przez gości;" : "2 desserts of guests' choice;",
    "napojów gorących;": "hot drinks;",
    "napojów alkoholowych." :"alcoholic beverages.",

    "Twoim zadaniem jest dobranie aperitifu, który możesz podać gościom oczekującym na przybycie wszystkich zaproszonych gości. Aperitif pełni nie tylko funkcję towarzyską, ale jest również serwowany przed posiłkiem w celu pobudzenia apetytu, gdyż wpływa na produkcję kwasów żołądkowych. Musisz pamiętać, że niektórzy goście mogą wybrać aperitif bezalkoholowy.":"Your task is to select an aperitif that you can serve to guests waiting for all the invited guests to arrive. The aperitif not only has a social function, but is also served before the meal to stimulate the appetite as it affects the production of stomach acids. You must keep in mind that some guests may opt for a non-alcoholic aperitif.",
    "Po wybraniu aperitifu, Twoim zadaniem jest skomponowanie bufetu, który składać się będzie z:":"Once you have chosen an aperitif, your task is to compose the buffet,  which will consist of:",
    "5 zimnych przystawek,":"5 cold starters,",
    "5 ciepłych przystawek,":"5 hot starters",
    "2 sałatek,":"2 salads",
    "oraz różnych dodatków.":"and various side dishes",
    "Pamiętaj, że to uroczysta kolacja w tradycyjnej polskiej restauracji. Wybór przystawek powinien być dopasowany do oferty kuchni polskiej, ale także do wieku gości i okoliczności imprezy. Przystawki będą podawane na głównym stole, na większych półmiskach. Zwróć uwagę na poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu.":"Keep in mind that this is a formal dinner in a traditional Polish restaurant. The choice of starters should be tailored to the Polish cuisine on offer, but also to the age of the guests and the circumstances of the event. Starters will be served on the main table, on larger platters. Pay attention to the correct combinations of dishes with starchy and vitamin supplements, desserts, drinks. All additional information can be found in the lower menu.",
    // 4
    "Planowana jest uroczysta kolacja, w której będzie brać udział 12 dorosłych osób. Uroczysta kolacja odbywa się w tradycyjnej polskiej restauracji w piątkowy wieczór.Jest to przyjęcie zasiadane z pełną obsługą kelnerską, czyli zimne i ciepłe przystawki, dania główne, desery i napoje są serwowane gościom na stół." : "The gala dinner is planned, which will be attended by 12 adults. The gala dinner takes place in a traditional Polish restaurant on a Friday evening. It is a sit-down party with full waiter service, i.e.,cold and hot starters, main courses, desserts and drinks are served to the guests at the table.",

    "tzw. bufetu (zimnych i ciepłych przystawek);" : "so-called buffet (cold and hot starters);",

    "Po wybraniu aperitifu, odpowiednich przystawek i dodatków musisz dobrać kolejno:":"Once you have selected an aperitif, appropriate starters and side dishes, you will have to select in turn:",
    "1 pierwsze danie,":"1 first course,",
    "3 dania główne do wyboru przez gości,":"3 main courses of your guestes' choice,",
    "2 desery do wyboru przez gości.":"2 desserts of your guest' choice",
    "Na koniec będziesz musiał również dokonać selekcji napojów zimnych, gorących oraz alkoholowych. Pamiętaj, że to uroczysta kolacja w tradycyjnej polskiej restauracji. Wybór dań powinien być dopasowany do oferty kuchni polskiej, ale także do wieku gości i okoliczności imprezy. Pierwsze danie podawane będzie w wazie, zaś drugie dania do wyboru wraz z dodatkami będą serwowane na dużych półmiskach. Dzięki temu każdy będzie mógł wybrać coś dla siebie i czuć się swobodnie.":"Finally, you will also need to make a selection of cold, hot and alcoholic drinks. Keep in mind that this is a formal dinner in a traditional Polish restaurant. The choice of dishes should be tailored to the Polish cuisine on offer, but also to the age of the guests and the circumstances of the event. The first course will be served in a tureen, while the second courses of your choice with side dishes will be served on large platters. This will allow everyone to choose something for themselves and feel at ease.",
    "Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu." : "In addition, the correct combinations of dishes with starchy and vitamin supplements, desserts, drinks should be taken into account. You will find all additional information in the lower menu."


  },
};
