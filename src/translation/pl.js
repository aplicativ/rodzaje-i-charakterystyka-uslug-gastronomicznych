export default {
common: {
  easy : "Impreza urodzinowa 18-stka poziom łatwy",
  average: "Impreza urodzinowa 18-stka poziom trudny",
  hard: "<span>Uroczysta <br/> kolacja poziom łatwy</span>",
  veryHard: "<span>Uroczysta kolacja <br/> poziom trudny</span>",

  "polecenie" : "polecenie",

    // Main course
    "Baza dań głównych": "Baza dań głównych",

    "Pierogi z jagodami":"Pierogi z jagodami",
    "Wołowina po burgundzku":"Wołowina po burgundzku",
    "Mandaryńskie placki":"Mandaryńskie placki",
    "Musaka":"Musaka",
    "Gnocchi z sosem serowym":"Gnocchi z sosem serowym",
    "Tagiatelle alla carbonara":"Tagiatelle alla carbonara",
    "Ravioli z borowikami":"Ravioli z borowikami",
    "Penne z kurczakiem i szpinakiem":"Penne z kurczakiem i szpinakiem",
    "Tofu z warzywami z woka":"Tofu z warzywami z woka",
    "Polędwiczki wieprzowe z ziemniakami":"Polędwiczki wieprzowe z ziemniakami",
    "Gyros z jagnięciny":"Gyros z jagnięciny",
    "Barszcz":"Barszcz",
    "Pizza Margherita":"Pizza Margherita",
    "Chłodnik litewski":"Chłodnik litewski",
    "Chiński smażony ryż z warzywami":"Chiński smażony ryż z warzywami",
    "Placek ziemniaczany z gulaszem z warzyw": "Placek ziemniaczany z gulaszem z warzyw",
    "Grillowana ośmiornica":"Grillowana ośmiornica",
    "Kaczka z jabłkami i sosem żurawinowym":"Kaczka z jabłkami i sosem żurawinowym",
    "Gulasz z dzika z plasterkami marchewki": "Gulasz z dzika z plasterkami marchewki",
    "Pielmieni ze szpinakiem":"Pielmieni ze szpinakiem",

    // First course
    "Baza dań pierwszych": "Baza dań pierwszych",

    "Zupa wonton z makaronem ryżowym":"Zupa wonton z makaronem ryżowym",
    "Barszcz czerwony z uszkami z nadzieniem z borowików": "Barszcz czerwony z uszkami z nadzieniem z borowików",
    "Gazpacho z jogurtem":"Gazpacho z jogurtem",

    // Desserts
    "Baza deserów": "Baza deserów",

    "Kutia": "Kutia",
    "Tiramisu": "Tiramisu",
    "Panna cotta":"Panna cotta",
    "Strudel jabłkowy":"Strudel jabłkowy",
    "Zupa owocowa":"Zupa owocowa",
    "Lody cytrynowe":"Lody cytrynowe",
    "Racuchy":"Racuchy",
    "Krem bawarski":"Krem bawarski",
    "Creme brulèe":"Creme brulèe",
    "Eklerka z czekoladą":"Eklerka z czekoladą",
    "Smażone banany w cieście": "Smażone banany w cieście",
    "Szarlotka z lodami":"Szarlotka z lodami",

    // Cold drink
    "Baza zimnych napojów": "Baza zimnych napojów",

    "Napój energetyczny":"Napój energetyczny",
    "Woda gazowana": "Woda gazowana",
    "Herbata":"Herbata",
    "Sok pomarańczowy": "Sok pomarańczowy",
    "Woda niegazowana":"Woda niegazowana",
    "Napój kolorowy gazowany":"Napój kolorowy gazowany",

    // Hot drinks
    "Baza ciepłych napojów": "Baza ciepłych napojów",

    "Piwo grzane": "Piwo grzane",
    "Espresso": "Espresso",
    "Herbata ziołowa": "Herbata ziołowa",
    "Mięta":"Mięta",
    "Herbata czarna":"Herbata czarna",

    // Alcohol
    "Baza napojów alkoholowych":"Baza napojów alkoholowych",

    "Wino białe wytrawne": "Wino białe wytrawne",
    "Miód pitny":"Miód pitny",
    "Wódka": "Wódka",
    "Likier cytrynowy": "Likier cytrynowy",
    "Piwo":"Piwo",
    "Krem czekoladowo-wiśniowy":"Krem czekoladowo-wiśniowy",
    "Wino czerwone półwytrawne": "Wino czerwone półwytrawne",

    // Aperitif
    "Baza aperitifów": "Baza aperitifów",

    "Szampan": "Szampan",
    "Gin z tonikiem": "Gin z tonikiem",
    "Ziołowe digestivo": "Ziołowe digestivo",
    "Wermut z sokiem pomarańczowym": "Wermut z sokiem pomarańczowym",
    "Ajerkoniak": "Ajerkoniak",
    "Tonik z plasterkiem cytryny": "Tonik z plasterkiem cytryny",
    "Sok pomarańczowy":"Sok pomarańczowy",
    "Limoncello":"Limoncello",
    "Wino wytrawne": "Wino wytrawne",
    "Wino musujące": "Wino musujące",
    "Krem czekoladowy": "Krem czekoladowy",

    // Cake
    "Baza tortów": "Baza tortów",

    "Tort a la Sacher": "Tort a la Sacher",
    "Tort tiramisu": "Tort tiramisu",
    "Tort makowy": "Tort makowy",
    "Tort cytrynowy": "Tort cytrynowy",
    "Tort piernikowy": "Tort piernikowy",
    "Tort migdałowy": "Tort migdałowy",

    // Cold starters
    "Baza zimnych przystawek": "Baza zimnych przystawek",

    "Dolmadakia - greckie gołąbki":"Dolmadakia - greckie gołąbki",
    "Szynka z melonem":"Szynka z melonem",
    "Deska serow włoskich":"Deska serow włoskich",
    "Czarne oliwki":"Czarne oliwki",
    "Zielone oliwki":"Zielone oliwki",
    "Mozzarella z pomidorami, oliwą i bazylią":"Mozzarella z pomidorami, oliwą i bazylią",
    "Karp w galarecie":"Karp w galarecie",
    "Deska wędlin włoskich":"Deska wędlin włoskich",
    "Wędzona makrela":"Wędzona makrela",
    "Śledzie w śmietanie":"Śledzie w śmietanie",
    "Śledzie w sosie curry":"Śledzie w sosie curry",
    "Indyk w galarecie":"Indyk w galarecie",
    "Ryba po grecku":"Ryba po grecku",
    "Deska wędlin":"Deska wędlin",
    "Śledzie w śmietanie":"Śledzie w śmietanie",
    "Tatar wołowy":"Tatar wołowy",
    "Carpaccio":"Carpaccio",
    "Bruschetta":"Bruschetta",
    "Deska serów polskich":"Deska serów polskich",
    "Placki warzywne z cukinią i wędzonym pstrągiem":"Placki warzywne z cukinią i wędzonym pstrągiem",
    "Szynka z melonem":"Szynka z melonem",

    // Hot starters
    "Baza ciepłych przystawek": "Baza ciepłych przystawek",

    "Kalmary smażone":"Kalmary smażone",
    "Sycylijska caponata":"Sycylijska caponata",
    "Placki ziemniaczane":"Placki ziemniaczane",
    "Tortilla z mięsem mielonym":"Tortilla z mięsem mielonym",
    "Pierożki wonton":"Pierożki wonton",
    "Grillowane warzywa z oliwą":"Grillowane warzywa z oliwą",
    "Placek chaczapuri":"Placek chaczapuri",
    "Sajgonki":"Sajgonki",
    "Mule w sosie pomidorowym":"Mule w sosie pomidorowym",
    "Quiche lorraine":"Quiche lorraine",
    "Krokiety z pieczarkami":"Krokiety z pieczarkami",
    "Calzone":"Calzone",
    "Śliwki zawijane w boczku":"Śliwki zawijane w boczku",
    "Jajka faszerowane":"Jajka faszerowane",
    "Pierogi z grzybami":"Pierogi z grzybami",
    "Krokiety z mięsem":"Krokiety z mięsem",

    // Salad
    "Baza sałatek": "Baza sałatek",

    "Sałatka z serem kozim":"Sałatka z serem kozim",
    "Sałatka caprese":"Sałatka caprese",
    "Sałatka z oscypkiem":"Sałatka z oscypkiem",
    "Sałatka z grillowanym łososiem":"Sałatka z grillowanym łososiem",
    "Sałatka jarzynowa":"Sałatka jarzynowa",
    "Sałatka z kaszą gryczaną i warzywami":"Sałatka z kaszą gryczaną i warzywami",
    "Sałatka orientalna z krewetkami":"Sałatka orientalna z krewetkami",
    "Sałatka gyros":"Sałatka gyros",
    "Sałatka z pieczonego buraka z serem kozim":"Sałatka z pieczonego buraka z serem kozim",

    // Mixed toopings
    "Dodatki różne": "Dodatki różne",

    "Chipsy":"Chipsy",
    "Kluski śląskie":"Kluski śląskie",
    "Grissini":"Grissini",
    "Pieczywo jasne":"Pieczywo jasne",
    "Ziemniaki gotowane":"Ziemniaki gotowane",
    "Pieczywo ciemne":"Pieczywo ciemne",
    "Makaron smażony":"Makaron smażony",
    "Dynia w zalewie":"Dynia w zalewie",
    "Ryż":"Ryż",
    "Grzybki marynowane":"Grzybki marynowane",
    "Frytki":"Frytki",
    "Ogórki kiszone":"Ogórki kiszone",

    // Title 
    "Dania główne:":"Dania główne:",
    "Dania pierwsze:":"Dania pierwsze:",
    "Desery":"Desery:",
    "Zimne napoje:":"Zimne napoje:",
    "Ciepłe napoje:":"Ciepłe napoje:",
    "Napoje alkoholowe:":"Napoje alkoholowe:",
    "Aperitif":"Aperitif:",
    "Torty":"Torty:",
    "Zimne przystawki:":"Zimne przystawki:",
    "Ciepłe przystawki:":"Ciepłe przystawki:",
    "Sałatki":"Sałatki:",
    "Dodatki różne:":"Dodatki różne:",

    // Knowledge base
    "Przejdź do filmu edukacyjnego":"Przejdź do filmu edukacyjnego",
    "Przejdź do sekwencji filmowych":"Przejdź do sekwencji filmowych",
    "Przejdź do e-booka":"Przejdź do e-booka",

    "Kuchnia włoska jest bardzo tradycyjna i różnorodna oraz słynie z wykorzystania świeżych składników (mięsa, ryb czy owoców morza), wielu warzyw (np. pomidorów, cebuli czy czosnku), oliwy z oliwek i przypraw (np. oregano, bazylia, pieprz, tymianek, rozmaryn). Tradycyjny posiłek składa się przystawki, pierwszego dania (np. makaronu lub rzadziej zupy), drugiego dania i deseru. 18-te urodziny to doskonała okazja, aby zorganizować je w formie przyjęcia koktajlowego, gdzie wszystkie wybrane dania będą serwowane na stołach bufetowych. Oto przykładowe kompozycje bufetu złożonego z: 4 zimnych przystawek, 2 ciepłych przystawek, 1 sałatki oraz różnych dodatków. Te dania możesz podać gościom, którzy przybędą świętować 18-te urodziny we włoskiej trattorii." : "Kuchnia włoska jest bardzo tradycyjna i różnorodna oraz słynie z wykorzystania świeżych składników (mięsa, ryb czy owoców morza), wielu warzyw (np. pomidorów, cebuli czy czosnku), oliwy z oliwek i przypraw (np. oregano, bazylia, pieprz, tymianek, rozmaryn). Tradycyjny posiłek składa się przystawki, pierwszego dania (np. makaronu lub rzadziej zupy), drugiego dania i deseru. 18-te urodziny to doskonała okazja, aby zorganizować je w formie przyjęcia koktajlowego, gdzie wszystkie wybrane dania będą serwowane na stołach bufetowych. Oto przykładowe kompozycje bufetu złożonego z: 4 zimnych przystawek, 2 ciepłych przystawek, 1 sałatki oraz różnych dodatków. Te dania możesz podać gościom, którzy przybędą świętować 18-te urodziny we włoskiej trattorii." ,

    "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie, zarówno tacy, którzy przepadają za mięsem oraz tacy, którzy preferują dietę wegetariańską. Warto zwrócić uwagę, aby w zestawie pojawiły się produkty pełnoziarniste, warzywa i ryby." : "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie, zarówno tacy, którzy przepadają za mięsem oraz tacy, którzy preferują dietę wegetariańską. Warto zwrócić uwagę, aby w zestawie pojawiły się produkty pełnoziarniste, warzywa i ryby.",

    "Dla kuchni włoskiej bardzo charakterystyczne są potrawy mączne (np. makarony, ravioli czy tortellini), które w towarzystwie sosów stają się dość kaloryczne. Dlatego może warto sięgnąć np. po makaron pełnoziarnisty lub wybrać ryby i warzywa.": "Dla kuchni włoskiej bardzo charakterystyczne są potrawy mączne (np. makarony, ravioli czy tortellini), które w towarzystwie sosów stają się dość kaloryczne. Dlatego może warto sięgnąć np. po makaron pełnoziarnisty lub wybrać ryby i warzywa.",

    "18-te urodziny to doskonała okazja, aby zorganizować je w formie przyjęcia koktajlowego, gdzie wszystkie wybrane dania będą serwowane na stołach bufetowych. Oto przykładowe kompozycje bufetu złożonego z: 2 dań głównych, deseru, zimnych napojów oraz tortu urodzinowego. Te dania możesz podać gościom, którzy przybędą świętować 18-te urodziny we włoskiej trattorii." :"18-te urodziny to doskonała okazja, aby zorganizować je w formie przyjęcia koktajlowego, gdzie wszystkie wybrane dania będą serwowane na stołach bufetowych. Oto przykładowe kompozycje bufetu złożonego z: 2 dań głównych, deseru, zimnych napojów oraz tortu urodzinowego. Te dania możesz podać gościom, którzy przybędą świętować 18-te urodziny we włoskiej trattorii.",

    "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie. Miłośnicy mięsa oraz wegetarianie powinni być zadowoleni. Warto zwrócić uwagę, aby nie pojawiały się kolorowe napoje gazowane, można też zastanowić się nad rezygnacją z deseru, skoro na koniec podany zostanie tort urodzinowy. Postarajmy się skomponować różnorodne menu." :"Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie. Miłośnicy mięsa oraz wegetarianie powinni być zadowoleni. Warto zwrócić uwagę, aby nie pojawiały się kolorowe napoje gazowane, można też zastanowić się nad rezygnacją z deseru, skoro na koniec podany zostanie tort urodzinowy. Postarajmy się skomponować różnorodne menu.",

    "Tradycyjna kuchnia polska oferuje duży repertuar różnie przygotowanych przystawek zimnych i ciepłych (np. śledzie, tatar, jajka faszerowane) oraz rozmaitych dodatków (np. ogórki kiszone, dynia czy grzybki marynowane)." :"Tradycyjna kuchnia polska oferuje duży repertuar różnie przygotowanych przystawek zimnych i ciepłych (np. śledzie, tatar, jajka faszerowane) oraz rozmaitych dodatków (np. ogórki kiszone, dynia czy grzybki marynowane).",

    "Uroczysta kolacja to doskonała okazja, aby przyjęcie miało charakter zasiadany. Warto zatem pomyśleć o odpowiednio dobranej zastawie i bieliźnie stołowej, a także eleganckiej dekoracji stołów. Goście obsługiwani będą przez kelnerów." :  "Uroczysta kolacja to doskonała okazja, aby przyjęcie miało charakter zasiadany. Warto zatem pomyśleć o odpowiednio dobranej zastawie i bieliźnie stołowej, a także eleganckiej dekoracji stołów. Goście obsługiwani będą przez kelnerów.",

    "Oto przykładowa kompozycja bufetu złożonego z: 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatek oraz różnych dodatków. Te dania możesz podać gościom, którzy przybędą na uroczystą kolację do tradycyjnej polskiej restauracji." :  "Oto przykładowa kompozycja bufetu złożonego z: 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatek oraz różnych dodatków. Te dania możesz podać gościom, którzy przybędą na uroczystą kolację do tradycyjnej polskiej restauracji.",

    "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie, zarówno tacy, którzy przepadają za mięsem oraz tacy, którzy preferują dietę wegetariańską. Warto zwrócić uwagę, aby w zestawie pojawiły się produkty pełnoziarniste, warzywa i ryby." : "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie, zarówno tacy, którzy przepadają za mięsem oraz tacy, którzy preferują dietę wegetariańską. Warto zwrócić uwagę, aby w zestawie pojawiły się produkty pełnoziarniste, warzywa i ryby.",

    "W tradycyjnej kuchni polskiej dominują potraw z mięsa i różnych warzyw (np. kapusty, ziemniaków), a także ciekawe desery (np. szarlotka, sernik czy pączki). Jej cechą charakterystyczną jest także duża oferta zup." : "W tradycyjnej kuchni polskiej dominują potraw z mięsa i różnych warzyw (np. kapusty, ziemniaków), a także ciekawe desery (np. szarlotka, sernik czy pączki). Jej cechą charakterystyczną jest także duża oferta zup.",

    "Uroczysta kolacja to doskonała okazja, aby przyjęcie miało charakter zasiadany. Warto zatem pomyśleć o odpowiednio dobranej zastawie i bieliźnie stołowej, a także eleganckiej dekoracji stołów. Goście obsługiwani będą przez kelnerów." :"Uroczysta kolacja to doskonała okazja, aby przyjęcie miało charakter zasiadany. Warto zatem pomyśleć o odpowiednio dobranej zastawie i bieliźnie stołowej, a także eleganckiej dekoracji stołów. Goście obsługiwani będą przez kelnerów." ,

    "Oto przykładowa kompozycja menu złożonego z: 1 pierwszego dania, 3 dań głównych do wyboru; 2 deserów do wyboru oraz napojów zimnych, gorących oraz alkoholowych. Te dania możesz podać gościom, którzy przybędą na uroczystą kolację do tradycyjnej polskiej restauracji." : "Oto przykładowa kompozycja menu złożonego z: 1 pierwszego dania, 3 dań głównych do wyboru; 2 deserów do wyboru oraz napojów zimnych, gorących oraz alkoholowych. Te dania możesz podać gościom, którzy przybędą na uroczystą kolację do tradycyjnej polskiej restauracji.",

    "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie. Miłośnicy mięsa oraz wegetarianie powinni być zadowoleni. Warto zwrócić uwagę, aby nie pojawiały się kolorowe napoje gazowane. Postarajmy się skomponować różnorodne menu." :  "Dzięki takiej kompozycji wszyscy goście znajdą coś smakowitego dla siebie. Miłośnicy mięsa oraz wegetarianie powinni być zadowoleni. Warto zwrócić uwagę, aby nie pojawiały się kolorowe napoje gazowane. Postarajmy się skomponować różnorodne menu.",

    "Zestaw nr 1" :  "Zestaw nr 1",
    "Zestaw nr 2" :  "Zestaw nr 2",
    "Bufet nr 1" :  "Bufet nr 1",

    // Feedback

     'error1' : "Jeśli miałeś trudności z wykonaniem zadania, wróć do instrukcji, a następnie spróbuj wykonać je jeszcze raz. Zwróć uwagę, czy nie tylko wybrałeś odpowiednie przystawki zimne, ciepłe, sałatki i dodatki, ale również czy ich liczba zgadza się z zamówionym menu. Zastanów się, czy wszystkie wybrane przez Ciebie składowe menu odpowiadają okolicznościom i miejscu, w którym odbywa się impreza urodzinowa. Być może wybrałeś dania, które nie są typowe dla kuchni włoskiej (np. śledzie w śmietanie, placek chaczapuri czy sałatka z oscypkiem). Następnym razem na pewno się uda!",

     'error2' : "Jeśli miałeś trudności z wykonaniem zadania, wróć do instrukcji, a następnie spróbuj wykonać je jeszcze raz. Zwróć uwagę, czy nie tylko wybrałeś odpowiednie dania główne, deser, napoje zimne oraz tort urodzinowy, ale również czy ich liczba zgadza się z zamówionym menu. Zastanów się, czy wszystkie wybrane przez Ciebie składowe menu odpowiadają okolicznościom i miejscu, w którym odbywa się impreza urodzinowa. Być może wybrałeś dania, które nie są typowe dla kuchni włoskiej (np. barszcz, gyros z jagnięciny czy kutia). Następnym razem na pewno się uda!",

     'error3' : "Jeśli miałeś trudności z wykonaniem zadania, wróć do instrukcji, a następnie spróbuj wykonać je jeszcze raz. Zwróć uwagę, czy nie tylko wybrałeś odpowiednie przystawki zimne, ciepłe, sałatki i dodatki, ale również czy ich liczba zgadza się z zamówionym menu. Zastanów się, czy wszystkie wybrane przez Ciebie składowe menu odpowiadają okolicznościom i miejscu, w którym odbywa się impreza urodzinowa. Być może wybrałeś dania, które nie są typowe dla kuchni polskiej (np. dolmadakia – greckie gołąbki, sajgonki czy sałatka caprese). Następnym razem na pewno się uda!",

     'error4' : "Jeśli miałeś trudności z wykonaniem zadania, wróć do instrukcji, a następnie spróbuj wykonać je jeszcze raz. Zwróć uwagę, czy nie tylko wybrałeś odpowiednie pierwsze i drugie dania, deser, a także zestaw napojów zimnych, ciepłych i alkoholowych, ale również czy ich liczba zgadza się z zamówionym menu. Zastanów się, czy wszystkie wybrane przez Ciebie składowe menu odpowiadają okolicznościom i miejscu, w którym odbywa się impreza urodzinowa. Być może wybrałeś dania, które nie są typowe dla kuchni polskiej (np. gazpacho z jogurtem, chiński smażony ryż czy krem bawarski). Następnym razem na pewno się uda!",

    "Niestety, nie wszystko poszło dobrze. Bufet na imprezę urodzinową we włoskiej trattoria jest niekompletny. Zwróć uwagę czy na pewno wybrałeś wszystkie przystawki zimne, ciepłe, sałatki i dodatki, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy." : "Niestety, nie wszystko poszło dobrze. Bufet na imprezę urodzinową we włoskiej trattoria jest niekompletny. Zwróć uwagę czy na pewno wybrałeś wszystkie przystawki zimne, ciepłe, sałatki i dodatki, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy.",

    "Ten poziom został wykonany poprawnie. Doskonale poradziłeś sobie z wyborem 4 zimnych i 2 ciepłych przystawek, 1 sałatki oraz dodatków części menu na imprezę urodzinową dla 8 osób w wieku 16-18 lat oraz 4 osób dorosłych, która odbywa się we włoskiej trattorii. Świetnie!" : "Ten poziom został wykonany poprawnie. Doskonale poradziłeś sobie z wyborem 4 zimnych i 2 ciepłych przystawek, 1 sałatki oraz dodatków części menu na imprezę urodzinową dla 8 osób w wieku 16-18 lat oraz 4 osób dorosłych, która odbywa się we włoskiej trattorii. Świetnie!",

    "Niestety, nie wszystko poszło dobrze. Główna część menu (dania główne, desery, napoje i  tort) na imprezę urodzinową we włoskiej trattoria jest niekompletna. Zastanów się czy na pewno wybrałeś wszystkie elementy menu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy." : "Niestety, nie wszystko poszło dobrze. Główna część menu (dania główne, desery, napoje i  tort) na imprezę urodzinową we włoskiej trattoria jest niekompletna. Zastanów się czy na pewno wybrałeś wszystkie elementy menu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy.",

    "Ćwiczenie zostało rozwiązane poprawnie. Poszczególne części menu imprezy urodzinowej dla 8 osób w wieku 16-18 lat oraz 4 osób dorosłych, która odbywa się we włoskiej trattorii, zostały prawidłowo skomponowane. Świetnie!" : "Ćwiczenie zostało rozwiązane poprawnie. Poszczególne części menu imprezy urodzinowej dla 8 osób w wieku 16-18 lat oraz 4 osób dorosłych, która odbywa się we włoskiej trattorii, zostały prawidłowo skomponowane. Świetnie!",

    "Niestety, nie wszystko poszło dobrze. Aperitif oraz bufet na uroczystą kolację w tradycyjnej polskiej restauracji są niekompletne. Zastanów się czy na pewno wybrałeś napoje na aperitif oraz wszystkie elementy bufetu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy." : "Niestety, nie wszystko poszło dobrze. Aperitif oraz bufet na uroczystą kolację w tradycyjnej polskiej restauracji są niekompletne. Zastanów się czy na pewno wybrałeś napoje na aperitif oraz wszystkie elementy bufetu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy.",

    "Pierwszy poziom został wykonany poprawnie. Zarówno 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatki oraz różne dodatki na uroczystą kolację dla 12 dorosłych osób w tradycyjnej polskiej restauracji w piątkowy wieczór zostały prawidłowo skomponowane. Doskonale poradziłeś sobie z tym zadaniem." : "Pierwszy poziom został wykonany poprawnie. Zarówno 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatki oraz różne dodatki na uroczystą kolację dla 12 dorosłych osób w tradycyjnej polskiej restauracji w piątkowy wieczór zostały prawidłowo skomponowane. Doskonale poradziłeś sobie z tym zadaniem.",

    "Niestety, nie wszystko poszło dobrze. Główna część menu (pierwsze danie, dania główne, desery oraz napoje) na uroczystą kolację w tradycyjnej polskiej restauracji jest niekompletne. Zastanów się czy na pewno wybrałeś wszystkie elementy menu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po  kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy." : "Niestety, nie wszystko poszło dobrze. Główna część menu (pierwsze danie, dania główne, desery oraz napoje) na uroczystą kolację w tradycyjnej polskiej restauracji jest niekompletne. Zastanów się czy na pewno wybrałeś wszystkie elementy menu, które zostały wskazane w poleceniu i czy ich liczba zgadza się z zamówionym menu. Jeśli zadanie sprawia Ci trudność, pamiętaj, że w dolnym menu znajdziesz również ikony, po  kliknięciu na które będziesz mógł wrócić do wiedzy dostępnej w innych multimediach e-materiału oraz skorzystać z bazy wiedzy.",

    "Ćwiczenie zostało rozwiązane poprawnie. Poszczególne części menu uroczystej kolacji dla 12 dorosłych osób w tradycyjnej polskiej restauracji w piątkowy wieczór, zostały prawidłowo skomponowane. Doskonale poradziłeś sobie z tym zadaniem." :"Ćwiczenie zostało rozwiązane poprawnie. Poszczególne części menu uroczystej kolacji dla 12 dorosłych osób w tradycyjnej polskiej restauracji w piątkowy wieczór, zostały prawidłowo skomponowane. Doskonale poradziłeś sobie z tym zadaniem.",

    // Instructions
    'instructionTitle':'Instrukcja obsługi do programu ćwiczeniowego „Układanie menu na przyjęcia okolicznościowe”',
    "instruction1":"Przed przystąpieniem do wykonania ćwiczenia zapoznaj się z poleceniem.",
    "instruction2":"Dla wygody korzystania z programu ćwiczeniowego kliknij ikonę trybu pełnoekranowego. Umożliwia on przeglądarce zajęcie całego ekranu.",
    "instruction3" :"W celu odsłuchania treści zawartych w programie ćwiczeniowym wybierz ikonę „Odsłuchaj”. W dolnym menu znajdziesz również ikony, po kliknięciu na które będziesz mógł zapoznać się z innymi materiałami multimedialnym z e-zasobu.",
    "instruction4": "Jeśli chcesz zapisać postępy swojej pracy użyj ikony „Pobierz listę kroków”, a plik zapisz na dysku komputera.",
    "instruction5" :"Program ćwiczeniowy umożliwia również zapisanie całej swojej pracy, wykonanie zrzutu ekranu oraz ponownego wykonania ćwiczenia. Po wykonaniu zadania, niezależnie od Twojego wyniku, otrzymasz informację zwrotną.",
    "instruction6" :"W przypadku korzystania wyłącznie z klawiatury należy użyć poniższych klawiszy:",
    "instructionList": "<ul><li>Tab - poruszanie się do przodu po elementach</li><li>Shift + Tab - poruszanie się do tyłu po elementach</li><li>Spacja - podnoszenie i upuszczanie elementów</li><li>Escape - anulowanie przeciągania</li><li>Strzałki - przenoszenie elementów do sąsiadujących stref upuszczania</li></ul>",

    // Recommendation
    // 1
    "Impreza urodzinowa 18-stka" : "Impreza urodzinowa 18-stka",

    "Planowana jest impreza z okazji 18 urodzin, w której będzie brać udział 8 osób w wieku 16-18 lat oraz 4 dorosłych. Impreza urodzinowa odbywa się we włoskiej restauracji w sobotę, między godziną 13:00 a 15:00. Jest to przyjęcie bufetowe, które pozwoli gościom na swobodny wybór menu, a także będzie sprzyjało rozmowom, zabawie, czy nawet tańcom." :"Planowana jest impreza z okazji 18 urodzin, w której będzie brać udział 8 osób w wieku 16-18 lat oraz 4 dorosłych. Impreza urodzinowa odbywa się we włoskiej restauracji w sobotę, między godziną 13:00 a 15:00. Jest to przyjęcie bufetowe, które pozwoli gościom na swobodny wybór menu, a także będzie sprzyjało rozmowom, zabawie, czy nawet tańcom.",

    "Zamówione urodzinowe menu będzie składało się z następujących elementów:":"Zamówione urodzinowe menu będzie składało się z następujących elementów:",

    "tzw. bufetu (zimnych i ciepłych przystawek oraz dodatków);" : "tzw. bufetu (zimnych i ciepłych przystawek oraz dodatków);",

    "2 dań głównych do wyboru przez gości;" : "2 dań głównych do wyboru przez gości;",
    "1 deseru;" : "1 deseru;",
    "napojów zimnych;": "napojów zimnych;",
    "tortu;":"tortu;",
    "i wina musującego do wzniesienia toastu urodzinowego.;" : "i wina musującego do wzniesienia toastu urodzinowego.",
    "POZIOM ŁATWY": "POZIOM ŁATWY",

    "Twoim zadaniem jest skomponowanie bufetu, który składać się będzie z: 4 zimnych przystawek, 2 ciepłych przystawek, 1 sałatki oraz różnych dodatków. Pamiętaj, że 18-te urodziny odbywają się we włoskiej restauracji. Wybór przystawek powinien być dopasowany do oferty kuchni włoskiej, ale także do wieku gości i okoliczności imprezy. Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu." : "Twoim zadaniem jest skomponowanie bufetu, który składać się będzie z: 4 zimnych przystawek, 2 ciepłych przystawek, 1 sałatki oraz różnych dodatków. Pamiętaj, że 18-te urodziny odbywają się we włoskiej restauracji. Wybór przystawek powinien być dopasowany do oferty kuchni włoskiej, ale także do wieku gości i okoliczności imprezy. Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu.",

    // 2
    "Planowana jest impreza z okazji 18 urodzin, w której będzie brać udział 8 osób w wieku 16-18 lat oraz 4 dorosłych. Impreza urodzinowa odbywa się we włoskiej restauracji w sobotę, między godziną 13:00 a 15:00. Jest to przyjęcie bufetowe, które pozwoli gościom na swobodny wybór menu, a także będzie sprzyjało rozmowom, zabawie, czy nawet tańcom." : "Planowana jest impreza z okazji 18 urodzin, w której będzie brać udział 8 osób w wieku 16-18 lat oraz 4 dorosłych. Impreza urodzinowa odbywa się we włoskiej restauracji w sobotę, między godziną 13:00 a 15:00. Jest to przyjęcie bufetowe, które pozwoli gościom na swobodny wybór menu, a także będzie sprzyjało rozmowom, zabawie, czy nawet tańcom.",

    "POZIOM TRUDNY": "POZIOM TRUDNY",

    "Po wybraniu odpowiednich przystawek i dodatków musisz dobrać 2 dania główne, deser, zimne napoje oraz tort. Pamiętaj, że 18-te urodziny odbywają się we włoskiej restauracji. Wybór dań głównych oraz deseru powinien być dopasowany do repertuaru kuchni włoskiej, ale także do wieku gości i okoliczności imprezy. Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu.": "Po wybraniu odpowiednich przystawek i dodatków musisz dobrać 2 dania główne, deser, zimne napoje oraz tort. Pamiętaj, że 18-te urodziny odbywają się we włoskiej restauracji. Wybór dań głównych oraz deseru powinien być dopasowany do repertuaru kuchni włoskiej, ale także do wieku gości i okoliczności imprezy. Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu.",

    // 3
    "Uroczysta kolacja" : "Uroczysta kolacja",

    "Planowana jest uroczysta kolacja, w której będzie brać udział 12 dorosłych osób. Uroczysta kolacja odbywa się w tradycyjnej polskiej restauracji w piątkowy wieczór.Jest to przyjęcie zasiadane z pełną obsługą kelnerską, czyli zimne i ciepłe przystawki, dania główne, desery i napoje są serwowane gościom na stół.": "Planowana jest uroczysta kolacja, w której będzie brać udział 12 dorosłych osób. Uroczysta kolacja odbywa się w tradycyjnej polskiej restauracji w piątkowy wieczór.Jest to przyjęcie zasiadane z pełną obsługą kelnerską, czyli zimne i ciepłe przystawki, dania główne, desery i napoje są serwowane gościom na stół.",

    "Zamówione menu będzie składało się z następujących elementów:" :"Zamówione menu będzie składało się z następujących elementów:",

    "zimnych i ciepłych przystawek, sałatek i dodatków;": "zimnych i ciepłych przystawek, sałatek i dodatków;",
    "1 pierwszego dania;":"1 pierwszego dania;",
    "3 dań głównych do wyboru przez gości;" : "3 dań głównych do wyboru przez gości;",
    "2 deserów do wyboru przez gości;" : "2 deserów do wyboru przez gości;",
    "napojów gorących;": "napojów gorących;",
    "napojów alkoholowych." :"napojów alkoholowych.",

    "Twoim zadaniem jest dobranie aperitifu, który możesz podać gościom oczekującym na przybycie wszystkich zaproszonych gości. Aperitif pełni nie tylko funkcję towarzyską, ale jest również serwowany przed posiłkiem w celu pobudzenia apetytu, gdyż wpływa na produkcję kwasów żołądkowych. Musisz pamiętać, że niektórzy goście mogą wybrać aperitif bezalkoholowy. Po wybraniu aperitifu, Twoim zadaniem jest skomponowanie bufetu, który składać się będzie z: 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatek oraz różnych dodatków. Pamiętaj, że to uroczysta kolacja w tradycyjnej polskiej restauracji. Wybór przystawek powinien być dopasowany do oferty kuchni polskiej, ale także do wieku gości i okoliczności imprezy. Przystawki będą podawane na głównym stole, na większych półmiskach. Zwróć uwagę na poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu." : "Twoim zadaniem jest dobranie aperitifu, który możesz podać gościom oczekującym na przybycie wszystkich zaproszonych gości. Aperitif pełni nie tylko funkcję towarzyską, ale jest również serwowany przed posiłkiem w celu pobudzenia apetytu, gdyż wpływa na produkcję kwasów żołądkowych. Musisz pamiętać, że niektórzy goście mogą wybrać aperitif bezalkoholowy. Po wybraniu aperitifu, Twoim zadaniem jest skomponowanie bufetu, który składać się będzie z: 5 zimnych przystawek, 5 ciepłych przystawek, 2 sałatek oraz różnych dodatków. Pamiętaj, że to uroczysta kolacja w tradycyjnej polskiej restauracji. Wybór przystawek powinien być dopasowany do oferty kuchni polskiej, ale także do wieku gości i okoliczności imprezy. Przystawki będą podawane na głównym stole, na większych półmiskach. Zwróć uwagę na poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu.",

    // 4
    "Planowana jest uroczysta kolacja, w której będzie brać udział 12 dorosłych osób. Uroczysta kolacja odbywa się w tradycyjnej polskiej restauracji w piątkowy wieczór.Jest to przyjęcie zasiadane z pełną obsługą kelnerską, czyli zimne i ciepłe przystawki, dania główne, desery i napoje są serwowane gościom na stół." : "Planowana jest uroczysta kolacja, w której będzie brać udział 12 dorosłych osób. Uroczysta kolacja odbywa się w tradycyjnej polskiej restauracji w piątkowy wieczór.Jest to przyjęcie zasiadane z pełną obsługą kelnerską, czyli zimne i ciepłe przystawki, dania główne, desery i napoje są serwowane gościom na stół.",

    "tzw. bufetu (zimnych i ciepłych przystawek);" : "zimnych i ciepłych przystawek, sałatek i dodatków;",

    "Po wybraniu aperitifu, odpowiednich przystawek i dodatków musisz dobrać kolejno 1 pierwsze danie, 3 dania główne do wyboru przez gości; 2 desery do wyboru przez gości. Na koniec będziesz musiał również dokonać selekcji napojów zimnych, gorących oraz alkoholowych. Pamiętaj, że to uroczysta kolacja w tradycyjnej polskiej restauracji. Wybór dań powinien być dopasowany do oferty kuchni polskiej, ale także do wieku gości i okoliczności imprezy. Pierwsze danie podawane będzie w wazie, zaś drugie dania do wyboru wraz z dodatkami będą serwowane na dużych półmiskach. Dzięki temu każdy będzie mógł wybrać coś dla siebie i czuć się swobodnie." : "Po wybraniu aperitifu, odpowiednich przystawek i dodatków musisz dobrać kolejno: 1 pierwsze danie, 3 dania główne do wyboru przez gości; 2 desery do wyboru przez gości. Na koniec będziesz musiał również dokonać selekcji napojów zimnych, gorących oraz alkoholowych. Pamiętaj, że to uroczysta kolacja w tradycyjnej polskiej restauracji. Wybór dań powinien być dopasowany do oferty kuchni polskiej, ale także do wieku gości i okoliczności imprezy. Pierwsze danie podawane będzie w wazie, zaś drugie dania do wyboru wraz z dodatkami będą serwowane na dużych półmiskach. Dzięki temu każdy będzie mógł wybrać coś dla siebie i czuć się swobodnie.",

    "Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu." : "Ponadto pod uwagę należy wziąć poprawne zestawienia potraw z dodatkami skrobiowymi i witaminowymi, deserami, napojami. Wszystkie dodatkowe informacje znajdziesz w dolnym menu."
  }
  
};
